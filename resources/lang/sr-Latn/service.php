<?php

return array(

	'title' 	=> 'Naš servisni tim Vam je na usluzi',
	'intro_1' 	=> 'Servisirajte svoje mašine sa nama i koristite našu servisnu podršku koja funkcioniše',
	'intro_2' 	=> '365 dana u godini.',
	'intro_3' 	=> 'Servisni tim',
	'intro_4' 	=> 'može vam pomoći u svim vrstama različitih problema koji se mogu javiti na CNC mašini.',
	'sub_1' 	=> 'Naš',
	'sub_2' 	=> 'Servisni tim',
	'sub_3' 	=> 'je specijalizovan u oblastima:',
	'list_1' 	=> 'Mehanike',
	'desc_1' 	=> 'dijagnostiku i zamenu oštećenih delova, protokolarno poravnanje ose, Renischaw Ballbar test',
	'list_2' 	=> 'Elektrike',
	'desc_2' 	=> 'ožičenje i dijagnostika prema električnim šemama, zamenu oštećenih električnih delova',
	'list_3' 	=> 'Elektronike',
	'desc_3_a'  => 'dijagnostiku elektronskih PCB ploča,',
	'desc_3_b'  => 'zamena oštećenih memorijskih čipova ili neispravnih elektronskih delova na PCB pločama,',
	'desc_3_c'  => 'učitavanje / preuzimanje beckup-a i parametara mašine',
	'list_4' 	=> 'Hidraulike i pneumatike',
	'desc_4' 	=> 'dijagnostika i zamena oštećenih delova',
	'list_5' 	=> 'PLC',
	'desc_5' 	=> 'dijagnostika i zamena oštećenih delova',
	'list_6' 	=> 'Softver',
	'desc_6' 	=> 'formatiranje, brisanje i upgrade vašeg softverskog sistema',
	'list_7' 	=> 'Rukovanje',
	'desc_7' 	=> 'obuka i dijagnostika vašeg operativnog sistema',
	'outro_1' 	=> 'Tokom decenija servisiranja različitih vrsta CNC mašina uspostavili smo širok spektar partnera u servisnom poslovanju. Ako neki problem ne možemo Mi rešiti, organizovaćemo posetu nekih od naših partnera koji imaju sposobnosti i iskustvo kako bi rešili vaš problem.',
	'outro_2' 	=> 'CAD / CAM tehnologija i postprocesori',
	'outro_3' 	=> ', koristite našeg softverskog inženjera da vam pomogne u vašem problemu.',
	'outro_4' 	=> 'Nudimo rešenja u CAD / CAM tehnologiji, obuci i pravilnu izradu postprocesora.'

);