<?php

return array(

	'result' 	=> 'Rezultat pretrage za',
	'total' 	=> 'Ukupno',
	'items'		=> 'mašina je pronađeno',
	'only' 		=> 'Samo',
	'item' 		=> 'mašina je pronađena',
	'mistype' 	=> 'Možda ste pogrešno ukucali termin za pretragu...',
	'min' 		=> 'Reč mora da sadrži minimum tri karaktera',

);

