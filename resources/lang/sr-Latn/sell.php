<?php


return array(

	'sell' 			=> 'Prodaja',

	'title_1' 		=> 'Kako prodati svoje korištene industrijske mašine širom sveta',
	'title_2' 		=> 'preko',
	'title_3' 		=> 'sajta!',

	'content_1' 	=> 'je online market',
	'content_2' 	=> 'Jednostavan i lak za upotrebu.',
	'content_3' 	=> 'Postavite svoje industrijske mašine online i kupci širom sveta će pronaći vašu mašinu na našoj veb stranici.',
	'content_4' 	=> 'Prodajte svoju mašinu ili kupite nove mašine kako biste se razvili u vašem sledećem poslovnom koraku.',
	'content_5' 	=> 'Obnovite mašinski park ili automatizujte proizvodnju sa ',

	'content_6' 	=> 'Zašto izabrati',
	'content_7' 	=> 'online berzu.',

	'content_8' 	=> 'Vaše ponude su vidljive i dostupne kupcima širom sveta ',
	'content_9' 	=> 'Koristite naš tim stručnjaka da pronađete najbolju ponudu ili najbolje kupce',
	'content_10' 	=> 'Implementirajte industriju 4.0 pomoću mašina sa našeg sajta ',
	'content_11' 	=> 'Ne samo da prodajemo mašine, već Vam nudimo i novo poslovno partnerstvo i mogućnosti za proširenje vašeg poslovanja',

	'content_12' 	=> 'Kako se možete registrovati i prodati svoje industrijske mašine kod nas? ',
	'content_13' 	=> 'Kontaktirajte nas',
	'content_14' 	=> 'i dostavite nam detalje vezane za Vašu mašinu',
	'content_15' 	=> 'mi ćemo oglas postaviti što je pre moguće, nakon što ga proveri naše osoblje',
	'content_16' 	=> 'Postavljanje oglasa je besplatno!',
	'content_17' 	=> 'Naš marketinški tim će promovisati vaše mašine i pronaćiće vam najboljeg kupca',
	'content_18'	=> 'Čim dobijemo validnu i potvrđenu ponudu, kontaktiraćemo vas i povezati vas sa kupcem da biste izvršili trgovinu',

	'outro_1' 		=> 'Ako iz bilo kog razloga',
	'outro_2' 		=> 'POMOĆ',
	'outro_3' 		=> 'vam je potrebna naši stručnjaci su tu 24/7 da vam pomognu',

	'outro_4' 		=> 'NAPREDUJMO ZAJEDNO!',

	'outro_5' 		=> 'testirajte našu posvećenost i performanse, jer',
	'outro_6' 		=> 'strogo radi po principu',
	'outro_7' 		=> 'DO',
	'outro_8' 		=> 'DONE',
	'outro_9' 		=> 'Ako ne nađemo kupca, ništa ne plaćate unapred.',
	'outro_10' 		=> 'MI imamo',
	'outro_11' 		=> 'najmanju',
	'outro_12' 		=> 'proviziju na tržištu!',

	'price' 		=> 'Cena u',
	'in' 			=> 'u',
	'commision' 	=> 'Provizija',
	'up_to'			=> 'Do',
	'to'			=> 'do',
	'over' 			=> 'i preko'

);