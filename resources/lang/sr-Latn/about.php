<?php


return array(

	'about' 		=> 'O nama',

	'title' 		=> 'Naš tim stručnjaka vam je na usluzi',

	'intro' 		=> 'Svi naši članovi tima su uključeni i rade u mašinskoj industriji. Ako imate tehnički zahtev ili nedoumicu, možemo vam ponuditi podršku u svim sferama CNC industrije. Ne oklevajte da nas kontaktirate, naš tim vam stoji',
	'intro_1' 		=> 'na raspolaganju.',

	'content_1' 	=> 'Naš',
	'content_2' 	=> 'Marketinški tim',
	'content_3' 	=> ' nudi podršku u oblastima:',

	'content_4' 	=> 'Prodaje i marketinga polovnih i novih CNC mašina ',
	'content_5' 	=> 'Dostupan je za sve vrste podrške u direktnoj prodaji sa kupcima',
	'content_6' 	=> 'Uređivanje i pripremu vaših polovnih mašina za prodaju',
	'content_7' 	=> 'Marketing vaših mašina i promovisanje vaše kompanije za proizvodnoj industriji',

	'content_8' 	=> 'Servisni tim',
	'content_9' 	=> 'Mehanike',
	'content_10'	=> 'Elektrike',
	'content_11' 	=> 'Elektronike',
	'content_12' 	=> 'Hidraulike i pneumatike',
	'content_13' 	=> 'PLC',
	'content_14' 	=> 'Softver',
	'content_15' 	=> 'Rukovanje',
	'content_16' 	=> 'CAD / CAM tehnologija',

	'content_17' 	=> 'U potrazi ste za novim poslovnim poduhvatom ili imate neiskorišćenu mašinu u vašoj proizvodnji ...',
	'content_18' 	=> 'Ne tražite više!',
	
	'content_19' 	=> 'Opustite se u udobnosti stolice i pretražujte na mreži od preko 1000 novih poslovnih mogućnosti.',
	'content_20' 	=> 'Pronađite posao sa našim partnerom',
	'content_21' 	=> 'Vodeća Evropska B2B online platforma za kupce i dobavljače za proizvodnju delova po meri.',
	'content_22' 	=> 'Brzo i lako odaberite odgovarajuće dobavljače za svoje pojedinačne delove u našoj bazi podataka koja broji više od 20.000 dobavljača.',

	'outro_1' 		=> 'Implementirajte industriju ',
	'outro_2' 		=> 'u vašoj fabrici uz pomoć našeg stručnog tima.',
	'outro_3' 		=> 'Kontaktirajte nas',
	'outro_4' 		=> 'sada i napravimo najbolju inovaciju i automatizaciju za vašu fabriku.',
	'outro_5' 		=> 'Zakoračite u budućnost...',
	'outro_6' 		=> 'Brendovi',

);