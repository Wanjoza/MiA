<?php

return array(

	'forging' 			=> 'KOVAČKE MAŠINE',
	'cold_forging' 		=> 'MAŠINE ZA HLADNO KOVANJE',
	'forging_hammers' 	=> 'MEHANIČKI KOVAČKI ČEKIĆI',
	'forging_presses' 	=> 'KOVAČKE PRESE',
	'screw_presses' 	=> 'VIJČANE PRESE'
);