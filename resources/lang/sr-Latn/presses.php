<?php

return array(

	'presses' 				=> 'PRESE',
	'eccentric' 			=> 'EKSCENTRIČNE PRESE',
	'hydraulic_mechanic' 	=> 'HIDRAULIČKE / MEHANIČKE PRESE',
	'stamping' 				=> 'ŠTAMPARSKE PRESE',
	'press_brake' 			=> 'PRESE ZA SAVIJANJE',
	'press_brake_cnc' 		=> 'CNC PRESE ZA SAVIJANJE',
	'punching' 				=> 'PANČ PRESE',
	'punching_cnc' 			=> 'CNC PANČ PRESE',
	'shears' 				=> 'MAKAZE / GILJOTINE',
	'shears_hydraulic' 		=> 'HIDRAULIČNE MAKAZE / GILJOTINE',
	'shears_mechanic'		=> 'MEHANIČKE MAKAZE / GILJOTINE'
);