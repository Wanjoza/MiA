<?php


return array(

	
	'caption_1'				=>	'Koristite našu veb stranicu da pronađete sve odgovarajuće mašine koje su vam potrebne za automatizaciju fabrike. Obratite se našem timu mašinskih stručnjaka i po vašem konkretnom zahtevu sprovodićemo potpunu automatizaciju vašeg sistema kako bi zadovoljili vaše zahteve.',
	'title_2'				=>	'ISKORISTITE NAŠE KONTAKTE, ZNANJE i bazu podataka',
	'caption_2'				=>	'Tokom decenije radili smo na uspostavljanju dobrih poslovnih kontakata i najboljih poslovnih rešenja za sve naše partnere u CNC industriji. Moto i fokus naše kompanije su povezivanje svih grana CNC industrije i potpune automatizacije vaše kompanije.',
	'title_3'				=>	'Naš poslovni partner',
	'caption_3'				=>	'"Techpilot - Pronađite pravog isporučioca za vaše delove po narudžbini brzo i jednostavno"',
	'title_4'				=>	'Dobra rešenja za vaš posao!',
	'caption_4'				=>	'Era industrije 4.0 je ovde! Prihvati je...',
	'what_we_offer'			=>	'Šta nudimo',
	'section_1_sub'			=>	'je vodeća kompanija u implementaciji INDUSTRIJE 4.0 u vašoj mašinskoj kompaniji',
	'section_1_caption'		=>	'Koristite našu veb stranicu da pronađete sve odgovarajuće mašine koje su vam potrebne za automatizaciju fabrike. Obratite se našem timu stručnjaka za obradu i po vašem konkretnom zahtevu sprovodićemo potpunu automatizaciju vašeg sistema kako bi zadovoljili vaše zahteve.',
	'section_2_title'		=>	'Najbolja investiciona rešenja u tehnologiju i mašineriju',
	'section_2_caption'		=>	'Industrijska revolucija 4.0 je uveliko zastupljena na vodećim svetskim tržištima. Automatizacija procesa proizvodnje vašeg preduzeća neophodna u konkuretnoj mašinskoj industriji. Važnu ulogu igra preciznost i brza isporuka završnih delova.',
	'section_3_title'		=>	'Povezujemo ljude, poslove i servisnu podršku',
	'section_3_sub'			=>	'na ujedinjenom i povezanom tržištu industrije 4.0',
	'section_3_caption'		=>	'Tokom decenije radili smo na uspostavljanju dobrih poslovnih kontakata i najboljih poslovnih rešenja za sve naše partnere u CNC industriji. Moto i fokus naše kompanije su povezivanje svih grana CNC industrije i potpune automatizacije vaše kompanije.',
	'section_4_title'		=>	'Iskoristite naše Kontakte, Znanje i bazu podataka',
	'section_4_caption'		=>	'Iskoristite naše kontakte sa partnerima širom sveta, naš tim servisera i tehničara će vam pomoći u svakom mogućem pogledu. Naši partneri su najbolji u svom poslu: automatizacija u pametnoj fabrici, B2B platformi za pronalaženje posla na mreži, servisna podrška i servisni tehničari, dijagnostika i popravka ploča PCB-a, dijagnostika vretena i servisiranje, direktno povezivanje sa proizvođačem mašine i kupovinom rezervnih delova direktno od dobavljača.',
	'explore_machines'		=>	'Istražite našu bazu podataka dostupnih mašina',
	'brands'				=>	'Brendovi'

);