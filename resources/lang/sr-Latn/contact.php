<?php


return array(

	'header_service_1'		=> 'Servisirajte Vaše mašine',
	'header_service_2'		=> 'sa nama i našim partnerima',
	'sub_service_1' 		=> 'Imate mašinu koja treba da se srvisira?',
	'sub_service_2' 		=> 'Ne ustručavajte se da nas kontaktirate.',
	'header_contact'		=> 'Kontaktirajte Nas',
	'sub_contact_1' 		=> 'Ukoliko imate bilo kakvih nedoumica ili pitanja, nemojte se ustručavati da Nas kontaktirate.',
	'sub_contact_2' 		=> 'Vaš zahtev će ostati anoniman, i Vaši podaci osigurani.',
	'header_sell' 			=> 'Mi radimo za zajednički profit!',
	'sub_sell_1' 			=> 'Da li ste zainteresovani za ovu mašinu... ',
	'sub_sell_2' 			=> 'ako jeste, ne ustručavajte se da nas kontaktirate.',
	'f_name' 				=> 'Ime',
	'l_name' 				=> 'Prezime',
	'company' 				=> 'Kompanija',
	'email' 				=> 'Email',
	'phone' 				=> 'Telefon',
	'title' 				=> 'Naslov',
	'message' 				=> 'Napišite poruku',
	'selected_machine' 		=> 'Odabrana mašina',
	'selected_trans' 		=> 'Odabrani prevoznik',
	'notice_1' 				=> 'Možemo da kontaktiramo odabranog prevoznika u Vaše ime',
	'notice_2' 				=> 'i da ugovorimo najpovoljniju moguću cenu za transport, kao i sve detalje vezane za isti.',
	'notice_3' 				=> '* molimo vas da napomenete u poruci da li vam je potrebna pomoć oko transporta.',
	'error' 				=> 'Greška sa izapranom stavkom...',
	'send_message' 			=> 'Pošaljite poruku',

);