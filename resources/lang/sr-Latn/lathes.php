<?php 


return array(

	'lathes' 				=> 'STRUGOVI',
	'vertical_turret'  		=> 'VERTIKALNI STRUGOVI',
	'horizintal_turret' 	=> 'HORIZONTALNI STRUGOVI',
	'automatic_lathes'  	=> 'AUTOMATSKI VIŠEVRETENI STRUGOVI',
	'swiss_type'  			=> 'SWISS TIP STRUGOVI'
);