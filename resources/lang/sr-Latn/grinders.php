<?php

return array(

	'grinders'					 => 'BRUSILICE',
	'cylindrical_centrless' 	 => 'CILINDRIČNE EXCENTRIČNE BRUSILICE',
	'cylindrical_ext' 			 => 'CILINDRIČNE BRUSILICE ZA UNUTRAŠNJE / SPOLJNO BRUŠENJE',
	'surface_grinders' 			 => 'BRUSILICE ZA POVRŠINSKO BRUŠENJE',

);