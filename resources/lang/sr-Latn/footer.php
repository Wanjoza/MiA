<?php


return array(

	'moto' 					=> 'Machine Innovation & Automation je vodeća kompanija u implementaciji INDUSTRIJE 4.0 u vašoj mašinskoj kompaniji!',
	'call_us'				=> 	'Pozovite nas ',
	'contact_us_today'		=>	'Kontaktirajte nas ',
	'send_an_email_on'		=>	'Pošaljite email na -',
	'visit_us'				=>	'Posetite nas - ',

);