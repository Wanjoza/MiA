<?php


return array(

	'no_photos'				=>	'Nema dostupnih slika!',
	'not_available'			=>	'Nema dostupnih mašina u ovom trenutku...',
	'year'					=>	'Godina',
	'location'				=>	'Lokacija',
	'price'					=>	'Cena',
	'model'					=>	'Model',
	'manufacturer'			=>	'Proizvođač',
	'category'				=>	'Kategorija',
	'id'					=>	'ID Proizvoda',
	'specifications'		=>	'Tehnički podaci',
	'description'			=>	'Opis',
	'more_offers'			=>	'Pogledajte jos ponuda u ovoj kategoriji!',

);