<?php


return array(

	'home' 						=> 	'Početna',
	'sell'						=> 	'Prodaja',
	'buy' 						=> 	'Kupovina',
	'service' 					=> 	'Servis',
	'about' 					=> 	'O nama',
	'contact' 					=>	'Kontakt',
	'search' 					=> 	'Pretraga',
	'language'					=>	'Jezik:',
	'cat_1_1'					=>	'MAŠINE ZA OBRADU',
	'cat_1_2'					=>	'METALA',
	'cat_2_1'					=>	'MAŠINE ZA OBLIKOVANJE /',
	'cat_2_2'					=>	'SEČENJE METALA',
	'cat_3_1'					=>	'MAŠINE ZA OBRADU',
	'cat_3_2'					=>	'PLASTIKE',
	'cat_4_1'					=>	'RAZNE INDUSTRIJSKE',
	'cat_4_2'					=>	'MAŠINE',
	'borers'					=>	'BORVERKE',
	'grinding_machines'			=>	'BRUSNE MAŠINE',
	'milling_machines'			=>	'GLODALICE',
	'machining_centers'			=>	'MAŠINSKI CENTRI',
	'lathes'					=>	'STRUGOVI',
	'bar_loaders'				=>	'DODAVAČI MATERIJALA',
	'forging_machines'			=>	'MAŠINE ZA KOVANJE',
	'presses'					=>	'PRESE',
	'laser'						=>	'MAŠINE ZA SEČENJE LASEROM',
	'plasma'					=>	'MAŠINE ZA SEČENJE PLAZMOM/GASOM',
	'waterjet'					=>	'MAŠINE ZA SEČENJE VODENIM MLAZOM',
	'blow_molding'				=>	'MAŠINE ZA KALUPLJENJE UDUVAVANJEM',
	'single_extruder'			=>	'EKSTRUDERI JEDNOSTRUKO-VIJČANI',
	'twin_extruder'				=>	'EKSTRUDERI DVOSTRUKO-VIJČANI ',
	'injestion_molding'			=>	'MAŠINE ZA BRIZGANJE PLASTIKE',
	'plastic_other'				=>	'RAZNE MAŠINE ZA OBRADU PLASTIKE',
	'recycling'					=>	'KOMPAKTORI / RECIKLATORI PLASTIKE',
	'compressors'				=>	'KOMPRESORI',
	'forklifts'					=>	'VILJUŠKARI',
	'cranes'					=>	'KRANOVI',
	'robots'					=>	'ROBOTI'

);