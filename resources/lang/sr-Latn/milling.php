<?php


return array(

	'milling' 					=> 'GLODALICE',
	'horizontal_miling' 		=> 'HORIZONTALNE GLODALICE',
	'horizontal_cnc' 			=> 'HORIZONTALNE CNC GLODALICE',
	'universal_milling' 		=> 'UNIVERZALNE GLODALICE',
	'universal_cnc' 			=> 'UNIVERZALNE CNC GLODALICE',
	'portal_milling' 			=> 'PORTALNE GLODALICE',
	'vertical_milling' 			=> 'VERTIKALNE GLODALICE',
	'vertical_cnc' 				=> 'VERTIKALNE CNC GLODALICE'
);