<?php


return array(

	'sell' 			=> 'Sell',

	'title_1' 		=> 'How to sell your used industrial machines worldwide',
	'title_2' 		=> 'with',
	'title_3' 		=> 'website!',

	'content_1' 	=> 'market place website is',
	'content_2' 	=> 'Easy to use and user friendly.',
	'content_3' 	=> 'Upload your industrial machines online and buyers worldwide will find your machine on our website.',
	'content_4' 	=> 'Sell your machinery or buy newer machines to evolve in your next business step.',
	'content_5' 	=> 'Innovate your machine park or automate your production with ',

	'content_6' 	=> 'Why choose',
	'content_7' 	=> 'to sell your used industrial machines.',

	'content_8' 	=> 'Your offers are marketed worldwide ',
	'content_9' 	=> 'Use our team of specialist to find best offer or best buyer',
	'content_10' 	=> 'Implement IDUSTRY 4.0 with machines from our website',
	'content_11' 	=> 'We do not just sell machines we also offer you new business partnership and opportunities to expand your business ',

	'content_12' 	=> 'How can you register and sell your industrial machines with US.',
	'content_13' 	=> 'Contact Us',
	'content_14' 	=> 'and provide us details with your machine',
	'content_15' 	=> 'and we will upload it as soon as possible, after it\'s checked by our staff',
	'content_16' 	=> 'Uploads are for free!',
	'content_17' 	=> 'Our team of specialist will promote your machines and find you a best buyer match',
	'content_18'	=> 'As soon as we receive valid and confirmed offer we will contact you and connect you to buyer to close the deal',

	'outro_1' 		=> 'For any',
	'outro_2' 		=> 'HELP',
	'outro_3' 		=> 'needed our specialist are here 24/7 to help you',

	'outro_4' 		=> 'LET\'S GROW TOGETHER',

	'outro_5' 		=> 'TEST our commitment and performance, because',
	'outro_6' 		=> 'is working strictly on',
	'outro_7' 		=> 'DO',
	'outro_8' 		=> 'DONE',
	'outro_9' 		=> 'If we don’t find you a buyer you don’t pay anything.',
	'outro_10' 		=> 'WE have the',
	'outro_11' 		=> 'smallest',
	'outro_12' 		=> 'commission on market!',

	'price' 		=> 'Your price in',
	'in' 			=> 'in',
	'commision' 	=> 'Commission',
	'up_to'			=> 'Up to',
	'to'			=> 'to',
	'over' 			=> 'and over'

);