<?php


return array(

	'about' 		=> 'About us',

	'title' 		=> 'Our team of specialist at your service',

	'intro' 		=> 'All our members of team are involved and working in tool machine industry. If you have any technical demand or misunderstanding, we can offer support in all CNC manufacturing business. Don’t hesitate to contact us, our team is available',
	'intro_1' 		=> 'just for you.',

	'content_1' 	=> 'Our',
	'content_2' 	=> 'Marketing Team',
	'content_3' 	=> ' is specialized in:',

	'content_4' 	=> 'Sales and marketing department of used and new CNC machines ',
	'content_5' 	=> 'Available for all kind of support in direct sales with buyers',
	'content_6' 	=> 'Arranging and updating your used machines for sale',
	'content_7' 	=> 'Marketing of your machine to buyers and most important to us promoting of your company manufacturing business',

	'content_8' 	=> 'Service Team',
	'content_9' 	=> 'Mechanics',
	'content_10'	=> 'Electrics',
	'content_11' 	=> 'Electronics',
	'content_12' 	=> 'Hydraulics & pneumatics',
	'content_13' 	=> 'PLC',
	'content_14' 	=> 'Software',
	'content_15' 	=> 'Operating',
	'content_16' 	=> 'CAD/CAM technology',

	'content_17' 	=> 'In demand for new business venture or do you have unused machine in your production...',
	'content_18' 	=> 'Search no more!',
	
	'content_19' 	=> 'Relax in comfort of your chair and search online from 1000 new business application.',
	'content_20' 	=> 'Find business with our partner',
	'content_21' 	=> 'Europe’s leading b2b online platform for buyers and suppliers of custom made parts.',
	'content_22' 	=> 'Quickly and easily select suitable suppliers for your custom made parts from more than 20,000 suppliers in our database.',

	'outro_1' 		=> 'Implement industry of ',
	'outro_2' 		=> 'in your factory with help of our specialist team.',
	'outro_3' 		=> 'Contact us',
	'outro_4' 		=> 'now and let’s make the best innovation and automation for your factory.',
	'outro_5' 		=> 'Enter the future...',
	'outro_6' 		=> 'Brands',

);