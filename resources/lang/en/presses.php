<?php

return array(

	'presses' 				=> 'PRESSES',
	'eccentric' 			=> 'ECCENTRIC PRESSES',
	'hydraulic_mechanic' 	=> 'HYDRAULIC / MECHANIC PRESSES',
	'stamping' 				=> 'STAMPING PRESSES',
	'press_brake' 			=> 'PRESS BRAKE',
	'press_brake_cnc' 		=> 'PRESS BRAKE NC /CNC',
	'punching' 				=> 'PUNCHING MACHINES',
	'punching_cnc' 			=> 'PUNCHING MACHINES CNC',
	'shears' 				=> 'SHEARS / GUILLOTINE',
	'shears_hydraulic' 		=> 'SHEARS / GUILLOTINE HYDRAULIC',
	'shears_mechanic' 		=> 'SHEARS / GUILLOTINE MECHANIC'
);