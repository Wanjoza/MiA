<?php


return array(

	'no_photos'				=>	'No Photos!',
	'not_available'			=>	'No available machines at the moment...',
	'year'					=>	'Year',
	'location'				=>	'Location',
	'price'					=>	'Price',
	'model'					=>	'Model',
	'manufacturer'			=>	'Manufacturer',
	'category'				=>	'Category',
	'id'					=>	'Product ID',
	'more_offers'			=>	'More offers in this category!',
	'specifications'		=>	'Specifications',
	'description'			=>	'Description',

);