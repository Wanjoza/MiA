<?php

return array(

	'forging' 			=> 'FORGING MACHINES',
	'cold_forging' 		=> 'COLD FORGING MACHINES',
	'forging_hammers' 	=> 'FORGING HAMMERS',
	'forging_presses' 	=> 'FORGING PRESSES',
	'screw_presses' 	=> 'SCREW PRESSES'
);