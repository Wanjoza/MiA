<?php 


return array(

	'lathes' 				=> 'LATHES',
	'vertical_turret'  		=> 'VERTICAL TURRET LATHE',
	'horizintal_turret' 	=> 'HORIZONTAL TURRET LATHE',
	'automatic_lathes'  	=> 'MULTISPINDLE AUTOMATIC LATHES',
	'swiss_type'  			=> 'SWISS TYPE LATHE'
);