<?php

return array(

	'title' 	=> 'Our service team is at your service',
	'intro_1' 	=> 'Service your machines with us and use our service support which operates',
	'intro_2' 	=> '365 days in the year.',
	'intro_3' 	=> 'Service department of',
	'intro_4' 	=> 'can help you in all kind of variety of different problems that can occur on your CNC machine.',
	'sub_1' 	=> 'Our',
	'sub_2' 	=> 'Service Team',
	'sub_3' 	=> 'is specialized in:',
	'list_1' 	=> 'Mechanics',
	'desc_1' 	=> 'diagnostic and exchange of broken parts, protocol alignment of axes, ballbar check',
	'list_2' 	=> 'Electrics',
	'desc_2' 	=> 'wiring and electrical diagram diagnostic, exchange of broken electrical parts',
	'list_3' 	=> 'Electronics',
	'desc_3_a'  => 'diagnostic of your electronic pc boards,',
	'desc_3_b'  => 'exchange of broken memory chips or faulty electronic parts on pc boards,',
	'desc_3_c'  => 'uploading/downloading back up and machine parameters',
	'list_4' 	=> 'Hydraulics & pneumatics',
	'desc_4' 	=> 'diagnostic and exchanging of your faulty equipment parts',
	'list_5' 	=> 'PLC',
	'desc_5' 	=> 'diagnostic and exchanging of faulty parts',
	'list_6' 	=> 'Software',
	'desc_6' 	=> 'formatting, cleaning and upgrade of your software system',
	'list_7' 	=> 'Operating',
	'desc_7' 	=> 'training and diagnostic of your operating system',
	'outro_1' 	=> 'Over the decades of servicing different kind of CNC machines we have established a wide range of partners in service business. If we can’t fix it, we will arrange a visit from some of our service partners who has capabilities and experience to fix your problem.',
	'outro_2' 	=> 'CAD/CAM technology & post processors',
	'outro_3' 	=> ', use our software engineer to help you in your problem.',
	'outro_4' 	=> 'We offer solutions in CAD/CAM, training and proper make of postprocessors.'

);