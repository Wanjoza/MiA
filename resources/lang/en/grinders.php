<?php

return array(

	'grinders' 					=> 'GRINDING MACHINES',
	'cylindrical_centrless' 	=> 'CYLINDRICAL CENTRLESS GRINDERS',
	'cylindrical_ext' 			=> 'CYLINDRICAL EXTERNAL / INTERNAL GRINDERS',
	'surface_grinders' 			=> 'SURFACE GRINDERS',

);