<?php


return array(

	'milling' 					=> 'MILLING MACHINES',
	'horizontal_miling' 		=> 'HORIZONTAL MILLING MACHINES',
	'horizontal_cnc' 			=> 'HORIZONTAL CNC MILLING MACHINES',
	'universal_milling' 		=> 'UNIVERSAL MILLING MACHINES',
	'universal_cnc' 			=> 'UNIVERSAL CNC MILLING MACHINES',
	'portal_milling' 			=> 'PORTAL MILLING MACHINES',
	'vertical_milling' 			=> 'VERTICAL MILLING MACHINES',
	'vertical_cnc' 				=> 'VERTICAL CNC MILLING MACHINES'
);