<?php

return array(

	'result' 	=> 'Search results for',
	'total' 	=> 'Total of',
	'items'		=> 'items found',
	'only' 		=> 'Only',
	'item' 		=> 'item found',
	'mistype' 	=> 'Maybe you\'ve mistyped name...',
	'min' 		=> 'You must enter minimum 3 characters',

);