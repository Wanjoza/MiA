<?php


return array(

	
	'caption_1'				=>	'Use our web site to find all the suitable machines that you require for your factory automation. Contact our team of machining specialist and on your specific request we will implement total automation of your system to satisfy your requirements',
	'title_2'				=>	'USE OUR CONTACTS, KNOW HOW & business data',
	'caption_2'				=>	'Over of decade we have worked to establish good business contacts and best business solutions to all our partners in CNC industry. Our company motto and focus is on connecting all branches of CNC industry and total automation of your company.',
	'title_3'				=>	'Our Business partner',
	'caption_3'				=>	'"Techpilot - Find the right supplier for your made-to-order parts quickly and simply"',
	'title_4'				=>	'Good solutions for your business!',
	'caption_4'				=>	'Era of industry 4.0 is here! Embrace it...',
	'what_we_offer'			=>	'What We Offer',
	'section_1_sub'			=>	'is the lead company in implementing INDUSTRY 4.0 in your machine tool company',
	'section_1_caption'		=>	'Use our web site to find all the suitable machines that you require for your factory automation. Contact our team of machining specialist and on your specific request we will implement total automation of your system to satisfy your requirements.',
	'section_2_title'		=>	'Best investment solutions in technology & machinery',
	'section_2_caption'		=>	'The Industrial Revolution 4.0 is largely represented on the world\'s leading markets. Automation of the production process of your company is necessary in the competitive machine industry. The precise and fast delivery of the finishing parts plays an important role.',
	'section_3_title'		=>	'Connecting PEOPLE, business & service support',
	'section_3_sub'			=>	'in sheared & connected market of industry 4.0',
	'section_3_caption'		=>	'Over of decade we have worked to establish good business contacts and best business solutions to all our partners in CNC industry. Our company motto and focus is on connecting all branches of CNC industry and total automation of your company.',
	'section_4_title'		=>	'Use our contacts, know how & business data',
	'section_4_caption'		=>	'Use our contacts with partner’s world wide, our team of service technicians will help you in any way possible. Our partners are hand picket as best in their business: from automation in smart factory, B2B platform to find business online, service support and service technician, PCB board diagnostic and repairing, spindle diagnostic and servicing, connect directly to machine manufacturer and buy spare parts directly from suppliers.',
	'explore_machines'		=>	'Explore our machine database',
	'brands'				=>	'Brands'

);