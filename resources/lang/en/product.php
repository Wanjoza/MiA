<?php 

return array(

	'contact_seller'	=> 'Contact Seller',
	'trans_title_1'		=> 'Worried about',
	'trans_title_2' 	=> 'transportation',
	'trans_title_3'	 	=> 'costs?',
	'trans_1' 			=> 'We have agreement with these transportation companies,',
	'trans_2' 			=> 'contact us on which one You prefer and we will arrange best possible transportation price for your desired machine.',
	'trans_3' 			=> '* OPTIONAL',

);