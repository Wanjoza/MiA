<?php


return array(

	'header_service_1'	=> 'Service your machines',
	'header_service_2'	=> 'with Us and our partners',
	'sub_service_1' 	=> 'Have machine that needs to be repaired...',
	'sub_service_2' 	=> 'Don\'t hesitate to contact us.',
	'header_contact'	=> 'Contact Us',
	'sub_contact_1' 	=> 'If You have any questions, please don\'t hesitate to contact us.',
	'sub_contact_2' 	=> 'Your request is anonymous and your data will be secured.',
	'header_sell'		=> 'We work for your profit!',
	'sub_sell_1' 		=> 'Are You interested in this machine... ',
	'sub_sell_2' 		=> 'Don\'t hesitate to contact us.',
	'f_name' 			=> 'First Name',
	'l_name' 			=> 'Last Name',
	'company' 			=> 'Company',
	'email' 			=> 'Email',
	'phone' 			=> 'Phone',
	'title' 			=> 'Title',
	'message' 			=> 'Write message',
	'selected_machine'  => 'Selected machine',
	'selected_trans' 	=> 'Selected transportation',
	'notice_1' 			=> 'We can contact this company on your behalf',
	'notice_2' 			=> 'and arrange transportation price and details.',
	'notice_3' 			=> '* please advise us in message if you need transportation.',
	'error' 			=> 'Error with selected item...',
	'send_message' 		=> 'Send Message',

);