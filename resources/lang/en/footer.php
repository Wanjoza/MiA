<?php


return array(

	'moto' 					=> 'Machine Innovation & Automation is the lead company in implementing INDUSTRY 4.0 in your machine tool company!',
	'call_us'				=> 'Call Us',
	'contact_us_today'		=>	'Contact Us Today',
	'send_an_email_on'		=>	'Send an Email on - ',
	'visit_us'				=>	'Visit Us -',

);