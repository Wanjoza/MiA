@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-primary">

                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">

                <div class="page-header">
                    <h1>Seller messages</h1>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        @if(count($item))
                            <div class="panel panel-info">
                                <div class="panel-heading"><b><span>{{ $item->manufacturer }}</span><span class="pull-right">{{ $item->year }}</span></b></div>
                                <div class="panel-body">
                                    <div class="col-md-12 col-sm-12">
                                        <h3 class="height center"><b>{{ $item->item_title }}</b></h3>
                                    </div>

                                    <div class="col-md-6 col-sm-12">
                                        @if(!empty($item->item_photos))
                                            @foreach($item->item_photos as $photo)
                                                @if($loop->first)
                                                    <div class="crop-lg">
                                                        <img class="img-responsive" src="{{ $photo->photo_path }}" alt="{{ $item->item_title }}">
                                                    </div>
                                                @endif
                                            @endforeach
                                        @else
                                            <h3 class="text-danger">No Photos!</h3>
                                        @endif
                                    </div>
                                    <div class="col-md-6 col-sm-12 table-data">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <th>Model</th>
                                                    <td>{{ $item->model }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Location</th>
                                                    <td>{{ $item->location }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Category</th>
                                                    <td>{{ $item->category->name }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Product id</th>
                                                    <td>{{ $item->product_id }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Price</th>
                                                    <td>{{ $item->price }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 center">
                                            <a href="/item/{{ $item->id }}" class="btn btn-primary contact-btn">Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        </div>
                    </div>

                    @if(!is_null($trans_src))
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">Transportation</b></div>

                                <div class="panel-body">
                                    <h3 class="center">Selected transportation</h3>
                                    <hr>
                                    <div class="crop-sm">
                                        <img src="{{ $trans_src }}">
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                    @endif
                    
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">From: <b>{{ $message->f_name }}</b> 
                                    <span class="pull-right"><b>{{ $message->created_at->diffForHumans() }}</b></span>
                                </div>
                                <div class="panel-body">
                                    <h3 class="center">{{ $message->title }}</h3>
                                    <hr>
                                    <p class="center lg-font"><i>{{ $message->message }}</i></p>

                                </div>
                                <div class="panel-footer">
                                    <h4>Details:</h4>
                                    <span>First name: <b>{{ $message->f_name }}</b></span><br>
                                    <span>Last name: <b>{{ $message->l_name }}</b></span><br>
                                    <span>Email: <b>{{ $message->email }}</b></span><br>
                                    <span>Phone: <b>{{ $message->phone }}</b></span><br>
                                    <span>Company: <b>{{ $message->company }}</b></span><br>
                                    <h5 class="right">{{$message->created_at->format('l jS \\of F Y')}}</h5>
                                </div>
                            </div>

                            {{-- Back --}}
                            <div class="row">
                                <div class="col-md-12 col-sm-12 center"><a href="/sellerMessage"><button class="btn btn-primary">Back</button></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection