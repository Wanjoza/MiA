<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	@include('layouts.partials.head')
</head>

	<body>


			@include('layouts.partials.nav')


			
			

			@include('layouts.partials.central')

			


			@include('layouts.partials.footer')


			

			@include('layouts.partials.scripts')
			
			@include('flash')
	</body>
</html>