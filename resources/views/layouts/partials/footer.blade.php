<footer class="footer">

    <!-- Footer Widget Section -->
    <div class="footer-widget-section">
        <div class="container text-center">
            <div class="row">
                <div class="col-sm-4 footer-block">
                    <div class="footer-widget widget_text">
                        <div class="footer-logo">
                            <a href="/home"><img src="/img/logo.png" alt="Machine Innovation & Automation is the lead company in implementing INDUSTRY 4.0 in your machine tool company!"></a>
                        </div>
                        <p>{{ trans('footer.moto') }}</p>

                    </div>
                </div><!-- /.col-sm-4 -->

                <div class="col-sm-4 footer-block">
                    <div class="footer-widget widget_text">
                        <h3>{{ trans('footer.contact_us_today') }}</h3>
                        <address>
                            {{ trans('footer.call_us') }} +381 63 610651 <br>
                            {{ trans('footer.send_an_email_on') }} <a href="mailto:#">office@machine.rs</a><br>
                            {{ trans('footer.visit_us') }} Bulevar Kralja Petra prvog 47<br>
                            21000 Novi Sad, Serbia<br>
                        </address>
                    </div>
                </div><!-- /.col-sm-4 -->

                <div class="col-sm-4 footer-block last">
                    <div class="footer-widget widget_text">
                        {{-- <div class="locale">
                            <h3>{{ trans('footer.choose_language') }}</h3>
                            <ul>
                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    <li>
                                        <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                            {{ $properties['native'] }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>   --}}
                    </div>
                </div><!-- /.col-sm-4 -->

            </div>
        </div>
    </div><!-- /.Footer Widget Section -->

    <div class="copyright-section">
        <div class="container clearfix">
                <span class="copytext">Copyright &copy; 2017 | <a href="/home">Machine Innovation & Automation</a></span>
        </div><!-- .container -->
    </div><!-- .copyright-section -->
</footer>
<!-- .footer -->

</div>
<!-- .content-wrapper -->
</div>
<!-- .offcanvas-pusher -->

<div class="uc-mobile-menu uc-mobile-menu-effect">
    <button type="button" class="close" aria-hidden="true" data-toggle="offcanvas"
            id="uc-mobile-menu-close-btn">&times;</button>
    <div>
        <div>
            <ul id="menu">
                <li><a href="/home">{{ trans('navbar.home') }}</a></li>
                <li><a href="/sell">{{ trans('navbar.sell') }}</a></li>
                <li><a href="/buy">{{ trans('navbar.buy') }}</a></li>
                <li><a href="/service">{{ trans('navbar.service') }}</a></li>
                <li><a href="/about">{{ trans('navbar.about') }}</a></li>
                <li><a href="/contact">{{ trans('navbar.contact') }}</a></li>
                <li>
                    <div class="box">
                      <div class="search-box sm-box">
                        <form method="GET" action="search">
                        <input type="search" name="search" id="search" placeholder="{{ trans('navbar.search') }}..." />
                            <button type="submit" class="icon"><i class="fa fa-search"></i></button>
                        </form>
                      </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- .uc-mobile-menu -->

</div>
<!-- #main-wrapper -->
