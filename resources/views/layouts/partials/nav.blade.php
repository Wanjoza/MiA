<div id="main-wrapper">
<!-- Page Preloader -->
<div id="preloader">
    <div id="status">
        <div class="status-mes"></div>
    </div>
</div>

<div class="uc-mobile-menu-pusher">

<div class="content-wrapper">
<nav class="navbar m-menu navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/home"><img src="/img/logo.png" alt="Machine Innovation & Automation is the lead company in implementing INDUSTRY 4.0 in your machine tool company!"></a>
        </div>

        <div class="container">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="#navbar-collapse-1">

                <ul class="nav navbar-nav navbar-right main-nav">
                    <li class="{{ Request::is('home') || Request::is('/')  ? 'active' : null }}"><a href="/home">{{ trans('navbar.home') }}</a></li>
                    
                    <li class="{{ Request::is('sell') ? 'active' : null }}"><a href="/sell">{{ trans('navbar.sell') }}</a></li>
                    <li class="dropdown m-menu-fw {{ Request::is('buy') ? 'active' : null }}"><a href="/buy"  class="dropdown-toggle">{{ trans('navbar.buy') }}
                        <span><i class="fa fa-angle-down"></i></span></a>
                        <ul class="dropdown-menu transparent">
                            <li>
                                <div class="m-menu-content">
                                    <ul class="col-sm-3">
                                         <div class="panel-group" id="cat1Scnd">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#cat1Scnd" href="#collapseCat1Scnd" aria-expanded="false" class="collapsed">{{ trans('navbar.cat_1_1') }} <br> {{ trans('navbar.cat_1_2') }}</a>
                                                    </h4>
                                                </div>
                                            </div>
                                            <div id="collapseCat1Scnd" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                <div class="panel-body">
                                                    <a href="/borers">{{ trans('navbar.borers') }}</a>
                                                    <a href="/grinding_machines">{{ trans('navbar.grinding_machines') }}</a>
                                                    <a href="/milling_machines">{{ trans('navbar.milling_machines') }}</a>
                                                    <a href="/machining_centers">{{ trans('navbar.machining_centers') }}</a>
                                                    <a href="/lathes">{{ trans('navbar.lathes') }}</a>
                                                    <a href="/products/10">{{ trans('navbar.bar_loaders') }}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="m-menu-content">
                                    <ul class="col-sm-3">
                                         <div class="panel-group" id="cat2Scnd">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#cat2Scnd" href="#collapseCat2Scnd" aria-expanded="false" class="collapsed">{{ trans('navbar.cat_2_1') }} <br> {{ trans('navbar.cat_2_2') }}</a>
                                                    </h4>
                                                </div>
                                            </div>
                                            <div id="collapseCat2Scnd" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                <div class="panel-body">
                                                    <a href="/forging_machines">{{ trans('navbar.forging_machines') }}</a>
                                                    <a href="/presses">{{ trans('navbar.presses') }}</a>
                                                    <a href="/products/13">{{ trans('navbar.laser') }}</a>
                                                    <a href="/products/14">{{ trans('navbar.plasma') }}</a>
                                                    <a href="/products/15">{{ trans('navbar.waterjet') }}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="m-menu-content">
                                    <ul class="col-sm-3">
                                         <div class="panel-group" id="cat3Scnd">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#cat3Scnd" href="#collapseCat3Scnd" aria-expanded="false" class="collapsed">{{ trans('navbar.cat_3_1') }}<br>{{ trans('navbar.cat_3_2') }}</a>
                                                    </h4>
                                                </div>
                                            </div>
                                            <div id="collapseCat3Scnd" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                <div class="panel-body">
                                                    <a href="/products/16">{{ trans('navbar.blow_molding') }}</a>
                                                    <a href="/products/17">{{ trans('navbar.single_extruder') }}</a>
                                                    <a href="/products/18">{{ trans('navbar.twin_extruder') }}</a>
                                                    <a href="/products/19">{{ trans('navbar.injestion_molding') }}</a>
                                                    <a href="/products/20">{{ trans('navbar.plastic_other') }}</a>
                                                    <a href="/products/21">{{ trans('navbar.recycling') }}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="m-menu-content">
                                    <ul class="col-sm-3">
                                         <div class="panel-group" id="cat4Scnd">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#cat4Scnd" href="#collapseCat4Scnd" aria-expanded="false" class="collapsed">{{ trans('navbar.cat_4_1') }}<br> {{ trans('navbar.cat_4_2') }}</a>
                                                    </h4>
                                                </div>
                                            </div>
                                            <div id="collapseCat4Scnd" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                <div class="panel-body">
                                                    <a href="/products/22">{{ trans('navbar.compressors') }}</a>
                                                    <a href="/products/23">{{ trans('navbar.forklifts') }}</a>
                                                    <a href="/products/24">{{ trans('navbar.cranes') }}</a>
                                                    <a href="/robots">{{ trans('navbar.robots') }}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="{{ Request::is('service') ? 'active' : null }}"><a href="/service">{{ trans('navbar.service') }}</a></li>
                    <li class="{{ Request::is('about') ? 'active' : null }}"><a href="/about">{{ trans('navbar.about') }}</a></li>
                    <li class="{{ Request::is('contact') ? 'active' : null }}"><a href="/contact">{{ trans('navbar.contact') }}</a></li>
                    <li>
                      <div class="search-box">
                        <form method="GET" action="search">
                            <input type="search" name="search" id="search" placeholder="{{ trans('navbar.search') }}..." />
                            <button type="submit" class="icon"><i class="fa fa-search"></i></button>
                        </form>
                      </div>
                    </li>
                    <li class="lang">
                        <span>{{ trans('navbar.language') }}</span>
                    </li>
                    <li>
                        <div class="locale">
                            <ul>
                                @if(App::getLocale() ==  'en')
                                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <li>
                                            <a  rel="alternate" 
                                                hreflang="{{ $localeCode }}" 
                                                href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}"
                                                {{ App::getLocale() ==  $localeCode  ? 'Class = selected' : 'Class = none' }}
                                                >

                                                {{ $properties['native'] }}
                                            </a>
                                        </li>
                                    @endforeach
                                @else
                                    @foreach(array_reverse(LaravelLocalization::getSupportedLocales()) as $localeCode => $properties)
                                    <li>
                                        <a  rel="alternate" 
                                            hreflang="{{ $localeCode }}" 
                                            href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}"
                                            {{ App::getLocale() ==  $localeCode  ? 'Class = selected' : 'Class = none' }}
                                            >

                                            {{ $properties['native'] }}
                                        </a>
                                    </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>  
                    </li>
                </ul>

            </div>
            <!-- .navbar-collapse -->
        </div>
    </div>
    <!-- .container -->
</nav>
<!-- .nav -->