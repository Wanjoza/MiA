@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-primary">

                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">

                <div class="page-header">
                    <h1>Contact form messages</h1>
                </div>

                    <div class="col-md-12 col-sm-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">From: <b>{{ $message->f_name }}</b> 
                                <span class="pull-right"><b>{{ $message->created_at->diffForHumans() }}</b></span>
                            </div>
                            <div class="panel-body">
                                <h3 class="center">{{ $message->title }}</h3>
                                <hr>
                                <p class="center lg-font"><i>{{ $message->message }}</i></p>

                            </div>
                            <div class="panel-footer">
                                <h4>Details:</h4>
                                <span>First name: <b>{{ $message->f_name }}</b></span><br>
                                <span>Last name: <b>{{ $message->l_name }}</b></span><br>
                                <span>Email: <b>{{ $message->email }}</b></span><br>
                                <span>Phone: <b>{{ $message->phone }}</b></span><br>
                                <span>Company: <b>{{ $message->company }}</b></span><br>
                                <h5 class="right">{{$message->created_at->format('l jS \\of F Y')}}</h5>
                            </div>
                        </div>

                        {{-- Back --}}
                        <div class="row">
                            <div class="col-md-12 col-sm-12 center"><a href="/contactMessage"><button class="btn btn-primary">Back</button></a></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection