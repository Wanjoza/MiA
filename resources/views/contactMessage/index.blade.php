@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-primary">

                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">

                <div class="page-header">
                    <h1>Contact form messages</h1>
                </div>

                    <div class="col-md-12 col-sm-12">
                        @if(count($messages))
                            @foreach($messages as $message)
                                <div class="panel panel-info">
                                    <div class="panel-heading">From: <b>{{ $message->f_name }}</b> 
                                        <span class="pull-right"><b>{{ $message->created_at->diffForHumans() }}</b></span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-md-6 col-sm-12">
                                            <a href="/contactMessage/{{ $message->id }}"><h3>{{ $message->title }}</h3></a>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <form action="/contactMessage/{{$message->id}}" method="POST" class="btn pull-right">
                                                {!! csrf_field() !!}{{method_field('DELETE')}}
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="panel panel-info">
                                <div class="panel-heading">Contact form Messages</div>
                                <div class="panel-body">
                                    <h3 class="text-danger center">There are no messages!...yet</h3>
                                </div>
                            </div>
                        @endif
                        {{-- Pagination --}}
                        <div class="row">
                            <div class="col-md-12 col-sm-12 center">{{ $messages->links() }}</div>
                        </div>
                        {{-- Back --}}
                        <div class="row">
                            <div class="col-md-12 col-sm-12"><a href="/admin"><button class="btn btn-primary">Back</button></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection