@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-primary">

                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">

                <div class="page-header">
                    <h1>{{ $title->name }}</h1>
                </div>

                    <div class="col-md-12 col-sm-12">
                        @if(count($items))
                            @foreach($items as $item)


                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="panel panel-info">
                                    <div class="panel-heading"><b><span>{{ $item->manufacturer }}</span><span class="pull-right">{{ $item->year }}</span></b></div>
                                    <div class="panel-body">
                                        <div class="col-md-12 col-sm-12">
                                            <p class="height"><b>{{ $item->item_title }}</b></p>
                                        </div>

                                        <div class="col-md-12 col-sm-12">
                                            <a href="/item/{{ $item->id }}">
                                                @if(!empty($item->item_photos))
                                                    @foreach($item->item_photos as $photo)
                                                        @if($loop->first)
                                                            <div class="crop">
                                                                <img src="{{ $photo->photo_path }}" alt="{{ $item->item_title }}">
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <h3 class="text-danger">No Photos!</h3>
                                                @endif
                                            </a>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <form action="/item/{{$item->id}}" method="POST" class="center">
                                                {!! csrf_field() !!}{{method_field('DELETE')}}
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <div class="panel panel-info">
                                <div class="panel-heading">{{ $title->name }}</div>
                                <div class="panel-body">
                                    <h3 class="text-danger center">There are no items!...yet</h3>
                                </div>
                            </div>
                        @endif
                        {{-- Pagination --}}
                        <div class="row">
                            <div class="col-md-12 col-sm-12 center">{{ $items->links() }}</div>
                        </div>
                        {{-- Back --}}
                        <div class="row">
                            <div class="col-md-12 col-sm-12"><a href="/admin"><button class="btn btn-primary">Back</button></a></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
