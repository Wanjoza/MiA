@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-primary">

                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">

                <div class="page-header">
                    <h1>Add items <small> / Add via CSV file</small></h1>
                </div>
                    <div class="row">
                       <div class="col-md-6 col-md-offset-3 col-sm-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">Upload CSV file</div>

                                <div class="panel-body">
                                    <form action="/csv/upload" method="post" enctype="multipart/form-data">
                                        {{csrf_field()}}

                                      <div class="col-md-12">
                                        <label for="imported_file">Select CSV file</label>
                                        <input type="file" name="imported_file" required />
                                        <hr>
                                      </div>
                                      <div class="col-md-12 center">
                                          <button class="btn btn-success" type="submit">Upload</button>
                                      </div>
                                    </form>
                                    @if (count($errors) > 0)
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 alert alert-danger" style="margin-top: 20px;">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>    
                <div class="page-header">
                    <h1>Add items <small> / Add via form</small></h1>
                </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">Add Form</div>

                                <div class="panel-body">
                                    <div class="form-group">
                                            <form method="POST" action="/item" class="form-group" enctype="multipart/form-data" multiple>
                                             {{ csrf_field() }}

                                             {{-- Main Info --}}

                                             <div class="col-md-6 col-sm-12 panel panel-default">
                                                <div class="panel-heading"><h2 class="text-primary">Main info</h2></div>
                                                <br>
                                                <div id="sel_1">
                                                    <label for="select_1">Choose category: </label>

                                                    <select name="select_1" id="select_1" class="form-control" required onchange="populate(this.id,'select_2')">
                                                        <option >--Select--</option>
                                                        <option value="1">METAL CUTTING MACHINES</option>
                                                        <option value="2">SHEET METAL SHAPING / CUTTING MACHINES</option>
                                                        <option value="3">PLASTIC PROCESSING MACHINES</option>
                                                        <option value="4">MISCELLANEOUS INDUSTRIAL MACHINES</option>
                                                        
                                                    </select>
                                                </div>
                                                <br>
                                                <div id="sel_2" >
                                                    <label for="select_2">Choose sub category: </label>

                                                    <select name="select_2" id="select_2" class="form-control" required onchange="populate(this.id,'select_3')">
                                                    </select>
                                                </div>
                                                <br>     
                                                <div id="sel_3" >
                                                    <label for="select_3">Choose sub category: </label>

                                                    <select name="category_id" id="select_3" class="form-control" required onchange="populateSpec(this.id,'category_spec')">
                                                    </select>
                                                </div>
                                                <br><br>

                                                <label for="item_title">Item title: </label>
                                                    <input type="text" name="item_title" class="form-control" value="{{ old('item_title') }}" placeholder="Enter title..." required>
                                                <br>

                                                <label for="manufacturer">Manufacturer: </label>
                                                    <input type="text" name="manufacturer" class="form-control" placeholder="Enter manufacturer..." value="{{ old('manufacturer') }}" required>
                                                <br>

                                                <label for="model">Model: </label>
                                                    <input type="text" name="model" class="form-control" value="{{ old('model') }}" placeholder="Enter model..." required>
                                                <br>

                                                <label for="year">Year: </label>
                                                    <input type="text" name="year" class="form-control" value="{{ old('year') }}" placeholder="Enter year..." required>
                                                <br>

                                                <label for="location">Location: </label>
                                                    <input type="text" name="location" class="form-control" value="{{ old('location') }}" placeholder="Enter location..." required>
                                                <br>

                                                <label for="product_id">Product ID: </label>
                                                    <input type="text" name="product_id" class="form-control" value="{{ old('product_id') }}" placeholder="Enter product ID..." required>
                                                <br>

                                                <label for="price">Price: </label>
                                                    <input type="text" name="price" class="form-control" value="{{ old('price') }}" placeholder="Enter price..." required>
                                                <br>

                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12">
                                                        <fieldset>
                                                            <legend>Choose photos: <b class="text-danger">(max-2mb/img)</b></legend>
                                                            <input type="file" name="item_photos[]" required multiple>
                                                            <br> 
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                
                                            </div>

                                            {{-- Specifications / Description --}}


                                            <div class="col-md-6 col-sm-12 panel panel-default" >
                                                <div class="panel-heading"><h2 class="text-primary">Specifications / Description</h2></div>
                                                <br>
                                                <div class="parent1">
                                                    <div id="category_spec">

                                                    </div>
                                                    
                                                    
                                                    {{-- General data --}}
                                                
                                                    <div>
                                                    <hr>
                                                        <label for="length_width_height">Length x width x height: </label>
                                                            <input type="text" name="length_width_height" value="n/a"  class="form-control"  placeholder="--- x --- x ---">
                                                        <br>
                                                    </div>
                                                    <div>
                                                        <label for="weight">Weight: </label>
                                                            <input type="text" name="weight" value="n/a" class="form-control"  placeholder="Weight...">
                                                        <br>
                                                    </div>
                                                    <div>
                                                        <label for="worked_hours">Worked hours: </label>
                                                            <input type="text" name="worked_hours" value="n/a" class="form-control"  placeholder="Worked hours...">
                                                        <br>
                                                    </div>
                                                    <div>
                                                        <label for="hours_under_power">Hours under power: </label>
                                                            <input type="text" name="hours_under_power" value="n/a" class="form-control"  placeholder="Hours under power...">
                                                        <br>
                                                    </div>
                                                    <div>
                                                        <label for="state">State: </label>
                                                            <input type="text" name="state" value="n/a" class="form-control"  placeholder="State...">
                                                        <br>
                                                    </div>
                                                    <div>
                                                        <label for="at_local_norms">At local norms: </label>
                                                            <input type="text" name="at_local_norms" value="n/a" class="form-control"  placeholder="At local norms...">
                                                        <br>
                                                    </div>
                                                    <div>
                                                        <label for="status">Status: </label>
                                                            <input type="text" name="status" value="n/a" class="form-control"  placeholder="Status...">
                                                        <br>
                                                    </div>

                                                </div>
                                            </div>
                                                <br>

                                                <div class="row" style="margin-top: 5%;">
                                                    <div class="col-md-12 col-sm-12 desc">
                                                        <legend>Description</legend>
                                                        <textarea class="col-md-offset-3" rows="8" name="description" placeholder="Description..." required></textarea>
                                                    </div>
                                                </div>

                                                <div class="row" style="margin-top: 5%;">
                                                    <div class="col-md-12 col-md-offset-5 col-sm-12">
                                                        <input type="submit" value="Upload item" class="btn btn-lg btn-success">
                                                    </div>
                                                </div>
                                            </form>

                                            @if (count($errors) > 0)
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @endif
                                    </div>
                                </div>
                            </div>
                            {{-- Back --}}
                            <div class="row">
                                <div class="col-md-12 col-sm-12"><a href="/admin"><button class="btn btn-primary">Back</button></a></div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".parent1").css("display","none");
    $('#select_3').change( function(){

        $(".parent1").slideDown(1500);
    });
    jQuery("#select_1 option:first-child").attr("selected", true);
</script>


@endsection
