@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-primary">

                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">

                <div class="page-header">
                    <h1>{{ $item->category->name }}</h1>
                </div>

                    <div class="col-md-12 col-sm-12">
                        @if(count($item))
                            <div class="panel panel-info">
                                <div class="panel-heading"><b><span>{{ $item->manufacturer }}</span><span class="pull-right">{{ $item->year }}</span></b></div>
                                <div class="panel-body">
                                    <div class="col-md-12 col-sm-12">
                                        <h3 class="height center"><b>{{ $item->item_title }}</b></h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            @if(!empty($item->item_photos))
                                                @foreach($item->item_photos as $photo)
                                                    <div class="col-md-4 col-sm-6 crop">
                                                        <img src="{{ $photo->photo_path }}" alt="{{ $item->item_title }}">
                                                    </div>
                                                @endforeach
                                            @else
                                                <h3 class="text-danger">No Photos!</h3>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6 table-data">
                                        <h3 class="title">General Info</h3>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <th>Manufacturer</th>
                                                    <td>{{ $item->manufacturer }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Model</th>
                                                    <td>{{ $item->model }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Year</th>
                                                    <td>{{ $item->year }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Location</th>
                                                    <td>{{ $item->location }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Category</th>
                                                    <td>{{ $item->category->name }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Product id</th>
                                                    <td>{{ $item->product_id }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Price</th>
                                                    <td>{{ $item->price }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-6 col-sm-12 specifications">
                                        <h3 class="title">Specifications</h3>
                                        <table>
                                            <tbody>
                                                @foreach($item_specs as $key => $val)
                                                    <tr>
                                                        <th>{{ $key }}</th>
                                                        <td>{{ $val }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 center mar-t-b-20">
                                            <a href="/item/{{ $item->id }}/edit">
                                                <button type="button" class="btn btn-success btn-lg">Edit Item</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="panel panel-info">
                                <div class="panel-body">
                                    <h3 class="text-danger center">There are no items!...yet</h3>
                                </div>
                            </div>
                        @endif
                        
                        {{-- Back --}}
                        <div class="row">
                            <div class="col-md-12 col-sm-12"><a href="{{URL::previous()}}"><button class="btn btn-primary">Back</button></a></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
