@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-primary">

                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    
                <div class="page-header">
                    <h1>Edit</h1>
                </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">Add Form</div>

                                <div class="panel-body">
                                    <div class="form-group">
                                            <form method="POST" action="/item/{{ $item->id }}" class="form-group" id="update_item" enctype="multipart/form-data" multiple>
                                             {{ csrf_field() }}{{method_field('PATCH')}}

                                             {{-- Main Info --}}

                                             <div class="col-md-6 col-sm-12 panel panel-default">
                                                <div class="panel-heading"><h2 class="text-primary">Main info</h2></div>
                                                <br>

                                                <input type="text" name="category_id" class="form-control hidden" value="{{ $item->category_id }}" required>

                                                <input type="text" name="url" class="form-control hidden" value="{{ $item->url }}">

                                                <br><br>

                                                <label for="item_title">Item title: </label>
                                                    <input type="text" name="item_title" class="form-control" value="{{ $item->item_title }}" placeholder="Enter title..." required>
                                                <br>

                                                <label for="manufacturer">Manufacturer: </label>
                                                    <input type="text" name="manufacturer" class="form-control" placeholder="Enter manufacturer..." value="{{ $item->manufacturer }}" required>
                                                <br>

                                                <label for="model">Model: </label>
                                                    <input type="text" name="model" class="form-control" value="{{ $item->model }}" placeholder="Enter model..." required>
                                                <br>

                                                <label for="year">Year: </label>
                                                    <input type="text" name="year" class="form-control" value="{{ $item->year }}" placeholder="Enter year..." required>
                                                <br>

                                                <label for="location">Location: </label>
                                                    <input type="text" name="location" class="form-control" value="{{ $item->location }}" placeholder="Enter location..." required>
                                                <br>

                                                <label for="product_id">Product ID: </label>
                                                    <input type="text" name="product_id" class="form-control" value="{{ $item->product_id }}" placeholder="Enter product ID..." required>
                                                <br>

                                                <label for="price">Price: </label>
                                                    <input type="text" name="price" class="form-control" value="{{ $item->price }}" placeholder="Enter price..." required>
                                                <br>

                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12">
                                                        <fieldset>
                                                            <legend>Add photos: <b class="text-danger">(max-2mb/img)</b></legend>
                                                            <input type="file" name="item_photos[]" multiple>
                                                            <br> 
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row"> 
                                                    @if(!empty($photos))
                                                        @foreach($photos as $photo)
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="col-md-8 col-sm-12">
                                                                <div class="crop">
                                                                    <img src="{{ $photo->photo_path }}" alt="{{ $item->item_title }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 col-sm-12">
                                                                <input type="checkbox" name="delete_photo[]" value="{{ $photo->id }}"><span><b> Delete</b></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12">
                                                            <hr>
                                                        </div>
                                                        @endforeach
                                                    @else
                                                        <h3 class="text-danger">No Photos!</h3>
                                                    @endif
                                                </div>
                                                
                                            </div>

                                            {{-- Specifications / Description --}}


                                            <div class="col-md-6 col-sm-12 panel panel-default" >
                                                <div class="panel-heading"><h2 class="text-primary">Specifications / Description</h2></div>
                                                <br>
                                                <div class="parent1">

                                                    <div id="category_spec">

                                                    </div>
                                                    
                                                    {{-- General data --}}
                                                
                                                    <div>
                                                    <hr>
                                                        <label for="length_width_height">Length x width x height: </label>
                                                            <input type="text" name="length_width_height" value="{{ $item->length_width_height }}"  class="form-control"  placeholder="--- x --- x ---">
                                                        <br>
                                                    </div>
                                                    <div>
                                                        <label for="weight">Weight: </label>
                                                            <input type="text" name="weight" value="{{ $item->weight }}" class="form-control"  placeholder="Weight...">
                                                        <br>
                                                    </div>
                                                    <div>
                                                        <label for="worked_hours">Worked hours: </label>
                                                            <input type="text" name="worked_hours" value="{{ $item->worked_hours }}" class="form-control"  placeholder="Worked hours...">
                                                        <br>
                                                    </div>
                                                    <div>
                                                        <label for="hours_under_power">Hours under power: </label>
                                                            <input type="text" name="hours_under_power" value="{{ $item->hours_under_power }}" class="form-control"  placeholder="Hours under power...">
                                                        <br>
                                                    </div>
                                                    <div>
                                                        <label for="state">State: </label>
                                                            <input type="text" name="state" value="{{ $item->state }}" class="form-control"  placeholder="State...">
                                                        <br>
                                                    </div>
                                                    <div>
                                                        <label for="at_local_norms">At local norms: </label>
                                                            <input type="text" name="at_local_norms" value="{{ $item->at_local_norms }}" class="form-control"  placeholder="At local norms...">
                                                        <br>
                                                    </div>
                                                    <div>
                                                        <label for="status">Status: </label>
                                                            <input type="text" name="status" value="{{ $item->status }}" class="form-control"  placeholder="Status...">
                                                        <br>
                                                    </div>

                                                </div>
                                            </div>
                                                <br>

                                                <div class="row" style="margin-top: 5%;">
                                                    <div class="col-md-12 col-sm-12 desc">
                                                        <legend>Description</legend>
                                                        <textarea class="col-md-offset-3" rows="8" name="description" placeholder="Description..." required>{{ $item->description }}</textarea>
                                                    </div>
                                                </div>

                                                <div class="row" style="margin-top: 5%;">
                                                    <div class="col-md-12 col-md-offset-5 col-sm-12">
                                                        <input type="submit" form="update_item" value="Update item" class="btn btn-lg btn-success">
                                                    </div>
                                                </div>
                                            </form>


                                            @if (count($errors) > 0)
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @endif
                                    </div>
                                </div>
                            </div>
                            {{-- Back --}}
                            <div class="row">
                                <div class="col-md-12 col-sm-12"><a href="/show_cat?category_id={{ $item->category->id }}"><button class="btn btn-primary">Back</button></a></div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    populateSpec('category_id','category_spec');
</script>


@endsection
