@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-primary">

                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">

                <div class="page-header">
                    <h1>Messages</h1>
                </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">Item related messages</div>

                            <div class="panel-body">
                                <a class="btn btn-primary" href="/sellerMessage">Show all</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">Service related messages</div>

                            <div class="panel-body">
                                <a class="btn btn-primary" href="/serviceMessage">Show all</a>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">Contact page related messages</div>

                            <div class="panel-body">
                                <a class="btn btn-primary" href="/contactMessage">Show all</a>
                            </div>
                        </div>
                    </div>
                    <br>
                <div class="page-header">
                    <h1>Items</h1>
                </div>

                    <div class="col-md-6 col-sm-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><b>Choose item</b></div>
                            <div class="form-group panel-body">
                                <form method="GET" action="/show_cat" class="form-group">

                                     {{-- Main Info --}}

                                     <div class="col-md-12 col-sm-12">
                                        <br>
                                        <div id="sel_1">
                                            <label for="select_1">Select category: </label>

                                            <select id="select_1" class="form-control" required onchange="populate(this.id,'select_2')">
                                                <option >--Select--</option>
                                                <option value="1">METAL CUTTING MACHINES</option>
                                                <option value="2">SHEET METAL SHAPING / CUTTING MACHINES</option>
                                                <option value="3">PLASTIC PROCESSING MACHINES</option>
                                                <option value="4">MISCELLANEOUS INDUSTRIAL MACHINES</option>
                                                
                                            </select>
                                        </div>
                                        <br>
                                        <div id="sel_2" >
                                            <label for="select_2">Select sub category: </label>

                                            <select id="select_2" class="form-control" required onchange="populate(this.id,'select_3')">
                                            </select>
                                        </div>
                                        <br>     
                                        <div id="sel_3" >
                                            <label for="select_3">Select sub category: </label>

                                            <select name="category_id" id="select_3" class="form-control" required onchange="populateSpec(this.id,'category_spec')">
                                            </select>
                                        </div>
                                        <div class="row" style="margin: 5%;">
                                            <div class="col-md-12 col-md-offset-4 col-sm-12">
                                                <input type="submit" value="Show Items" class="btn btn-primary">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">Add Items</div>

                            <div class="panel-body">
                                <a class="btn btn-success" href="/item/create">Add</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery("#select_1 option:first-child").attr("selected", true);
</script>
@endsection
