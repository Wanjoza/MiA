@extends('layouts.master')



@section('header')

<section class="repair-page-section page-title">
    <div class="container text-center">
        <h2>{{ trans('contact.header_service_1') }}<br>{{ trans('contact.header_service_2') }}</h2>
    </div>
</section>
<!-- .page-title -->

@stop





@section('content')
<section class="about-text pt-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>{{ trans('service.title') }}</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-12 about">
                <p>
                    {{ trans('service.intro_1') }} <span class="blue">24/7</span>, {{ trans('service.intro_2') }}<br>
                    {{ trans('service.intro_3') }} <b class="mia"><i>M</i>achine <i>I</i>nnovation & <i>A</i>utomation</b> {{ trans('service.intro_4') }}     
                </p><br>
                <hr>
                <p>
                    <span class="fn-md"> {{ trans('service.sub_1') }} <span class="blue">{{ trans('service.sub_2') }}</span> {{ trans('service.sub_3') }}</span><br><br>

                    <b class="blue">-</b>   <b>{{ trans('service.list_1') }}</b> 
                        <small>( {{ trans('service.desc_1') }} )</small>
                    <b class="blue">-</b><br>

                    <b class="blue">-</b>   <b>{{ trans('service.list_2') }}</b> 
                        <small>( {{ trans('service.desc_2') }} )</small>
                    <b class="blue">-</b><br>

                    <b class="blue">-</b>   <b>{{ trans('service.list_3') }}</b> 
                        <small>( {{ trans('service.desc_3_a') }} <br> {{ trans('service.desc_3_b') }} <br>  {{ trans('service.desc_3_c') }} )</small>
                    <b class="blue">-</b><br>

                    <b class="blue">-</b>   <b>{{ trans('service.list_4') }} </b> 
                        <small>( {{ trans('service.desc_4') }} )</small>
                    <b class="blue">-</b><br>

                    <b class="blue">-</b>   <b>{{ trans('service.list_5') }} </b> 
                        <small>( {{ trans('service.desc_5') }} )</small>
                    <b class="blue">-</b><br>

                    <b class="blue">-</b>   <b>{{ trans('service.list_6') }} </b> 
                        <small>( {{ trans('service.desc_6') }} )</small>
                    <b class="blue">-</b><br>

                    <b class="blue">-</b>   <b>{{ trans('service.list_7') }} </b> 
                        <small>( {{ trans('service.desc_7') }} )</small>
                    <b class="blue">-</b><br>
                </p>
                <hr>
                <p>
                     {{ trans('service.outro_1') }} 
                    
                </p><br>
                <p>
                    <span class="fn-m blue">{{ trans('service.outro_2') }} </span>{{ trans('service.outro_3') }} <br>
                    {{ trans('service.outro_4') }} 
                </p>
                <hr>
            </div>
        </div>
    </div>

</section>

<section class="contact-form ptb-50">
    <div class="container text-center">
        <h2>{{ trans('contact.sub_service_1') }}<br>{{ trans('contact.sub_service_2') }}</h2>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form method="POST" action="/serviceMessage" id="form" class="form-group">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group user-name">
                                <label for="f_name" class="sr-only">{{ trans('contact.f_name') }}</label>
                                <input type="text" class="form-control" name="f_name" value="{{ old('f_name') }}" required placeholder="{{ trans('contact.f_name') }}">
                            </div>
                            <div class="form-group user-name">
                                <label for="l_name" class="sr-only">{{ trans('contact.l_name') }}</label>
                                <input type="text" class="form-control" name="l_name" value="{{ old('l_name') }}" required  placeholder="{{ trans('contact.l_name') }}">
                            </div>

                            <div class="form-group user-company">
                                <label for="company" class="sr-only">{{ trans('contact.company') }}</label>
                                <input type="text" class="form-control" name="company" value="{{ old('company') }}" required placeholder="{{ trans('contact.company') }}">
                            </div>

                            <div class="form-group user-email">
                                <label for="email" class="sr-only">{{ trans('contact.email') }}</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="{{ trans('contact.email') }} Address">
                            </div>


                            <div class="form-group user-phone">
                                <label for="phone" class="sr-only">{{ trans('contact.phone') }}</label>
                                <input type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required placeholder="{{ trans('contact.phone') }}">
                            </div>
                        </div><!-- /.col-md-6 -->
                        <div class="col-md-6">
                            <div class="form-group user-title">
                                <label for="title" class="sr-only">{{ trans('contact.title') }}</label>
                                <input type="text" class="form-control" name="title" value="{{ old('title') }}" required placeholder="{{ trans('contact.title') }}">
                            </div>
                            <div class="form-group user-message">
                                <label for="message" class="sr-only">{{ trans('contact.message') }}</label>
                                <textarea class="form-control" name="message" value="{{ old('message') }}" required placeholder="{{ trans('contact.message') }}"></textarea>
                            </div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row-->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="captcha">
                                <div class="g-recaptcha" data-sitekey="6LeSVTQUAAAAAP3ap_Bzjy239MQfX9tC9LcVqgqe"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 ptb-20">
                            <input type="submit" class="btn btn-primary contact-btn" value="{{ trans('contact.send_message') }}">
                        </div>
                    </div>
                </form>

                @if (count($errors) > 0)
                <div class="row">
                    <div class="col-md-12 col-sm-12 alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
            </div><!-- /.col-md-8 -->
        </div>

    </div>
</section>
<!-- .contact-form-->

      
@stop

@section('page_script')


@stop