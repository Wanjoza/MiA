@extends('layouts.master')



@section('header')

<section class="sell-page-section page-title">
    <div class="container text-center">
        <h2>{{ trans('sell.sell') }}</h2>
    </div>
</section>
<!-- .page-title -->

@stop




@section('content')

 <section class="service-icon-style ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>{{ trans('sell.title_1') }}<br><small>{{ trans('sell.title_2') }} <b class="mia"><i>M</i>achine <i>I</i>nnovation & <i>A</i>utomation</b> {{ trans('sell.title_3') }}</small></h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>


    <div class="container text-center">
        <div class="row">
            <div class="col-sm-12 col-md-12 sell">
                
                <p>
                    <b class="mia"><i>M</i>achine <i>I</i>nnovation & <i>A</i>utomation</b> {{ trans('sell.content_1') }}<br>
                    {{ trans('sell.content_2') }}<br>
                    {{ trans('sell.content_3') }}<br> 
                    {{ trans('sell.content_4') }} <br>
                    {{ trans('sell.content_5') }}<b class="mia"><i>M</i>achine <i>I</i>nnovation & <i>A</i>utomation</b>.<br><br>
                </p>
                <p>
                    <span class="fn-md"> {{ trans('sell.content_6') }} <b class="mia"><i>M</i>achine <i>I</i>nnovation & <i>A</i>utomation</b> {{ trans('sell.content_7') }}</span><br><br>
                    <b class="blue">-</b>   {{ trans('sell.content_8') }} <b class="blue">-</b><br>
                    <b class="blue">-</b>   {{ trans('sell.content_9') }} <b class="blue">-</b><br>
                    <b class="blue">-</b>   {{ trans('sell.content_10') }} <b class="blue">-</b><br>
                    <b class="blue">-</b>   {{ trans('sell.content_11') }} <b class="blue">-</b><br>
                </p>
                <p>
                    <span class="fn-md"> {{ trans('sell.content_12') }}</span><br><br>
                    <b class="blue">-</b> <a href="contact">{{ trans('sell.content_13') }}</a> {{ trans('sell.content_14') }} <b class="blue">-</b><br>
                        <small>( {{ trans('sell.content_15') }} )</small><br>
                    <b class="blue">-</b>   {{ trans('sell.content_16') }} <b class="blue">-</b><br>
                    <b class="blue">-</b>   {{ trans('sell.content_17') }} <b class="blue">-</b><br>
                    <b class="blue">-</b>   {{ trans('sell.content_18') }} <b class="blue">-</b><br>
                </p>
                <hr>
                    <h2>{{ trans('sell.outro_1') }} <span class="blue">{{ trans('sell.outro_2') }}</span> {{ trans('sell.outro_3') }}</h2>
                <hr>
                <br>
                <p>
                    <span class="blue fn-lg">{{ trans('sell.outro_4') }}</span><br><br>

                    {{ trans('sell.outro_5') }} <span class="blue">M I <i class="gray">&</i> A</span> {{ trans('sell.outro_6') }} <span class="blue">{{ trans('sell.outro_7') }}</span> & <span class="blue"> {{ trans('sell.outro_8') }}</span>!<br> 
                    {{ trans('sell.outro_9') }}<br> 
                    {{ trans('sell.outro_10') }} <small class="blue">{{ trans('sell.outro_11') }}</small> {{ trans('sell.outro_12') }} 

                </p>
                <div class="commissions">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>{{ trans('sell.price') }} $</th>
                          <th>{{ trans('sell.in') }} €</th>
                          <th>{{ trans('sell.in') }} £</th>
                          <th>{{ trans('sell.commision') }}</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{{ trans('sell.up_to') }} $5,000</td>
                          <td>{{ trans('sell.up_to') }} €5,000</td>
                          <td>{{ trans('sell.up_to') }} £5,000</td>
                          <td>$/€/£200</td>
                        </tr>
                        <tr>
                          <td>$5,001 {{ trans('sell.to') }} $40,000</td>
                          <td>€5,001 {{ trans('sell.to') }} €40,000</td>
                          <td>£5,001 {{ trans('sell.to') }} £30,000</td>
                          <td>8 %</td>
                        </tr>
                        <tr>
                          <td>$40,001 {{ trans('sell.to') }} $80,000</td>
                          <td>€40,001 {{ trans('sell.to') }} €80,000</td>
                          <td>£30,001 {{ trans('sell.to') }} £60,000</td>
                          <td>7 %</td>
                        </tr>
                        <tr>
                          <td>$80,001 {{ trans('sell.to') }} $120,000</td>
                          <td>€80,001 {{ trans('sell.to') }} €120,000</td>
                          <td>£60,001 {{ trans('sell.to') }} £90,000/td>
                          <td>6 %</td>
                        </tr>
                        <tr>
                          <td>$120,001 {{ trans('sell.to') }} $160,000</td>
                          <td>€120,001 {{ trans('sell.to') }} €160,000</td>
                          <td>£90,001 {{ trans('sell.to') }} £120,000</td>
                          <td>5 %</td>
                        </tr>
                        <tr>
                          <td>$160,001 {{ trans('sell.to') }} $200,000</td>
                          <td>€160,001 {{ trans('sell.to') }} €200,000</td>
                          <td>£120,001 {{ trans('sell.to') }} £150,000</td>
                          <td>4 %</td>
                        </tr>
                        <tr>
                          <td>$200,001 {{ trans('sell.over') }}</td>
                          <td>€200,001 {{ trans('sell.over') }}</td>
                          <td>£150,001 {{ trans('sell.over') }}</td>
                          <td>3 %</td>
                        </tr>
                      </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>


</section>
<!-- .about-text-->



@stop

@section('page_script')


@stop