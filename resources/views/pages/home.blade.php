@extends('layouts.master')

@section('header')

<div id="x-corp-carousel" class="carousel slide hero-slide " data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#x-corp-carousel" data-slide-to="0" class="active"></li>
        <li data-target="#x-corp-carousel" data-slide-to="1"></li>
        <li data-target="#x-corp-carousel" data-slide-to="2"></li>
        <li data-target="#x-corp-carousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="/img/carousel-1.jpg" alt="Machine Innovation & Automation is the lead company in implementing INDUSTRY 4.0 in your machine tool company!">

            <div class="carousel-caption">
                <h1>Machine Innovation & Automation </h1><br>
                
                <p>{{ trans('home.caption_1') }}</p>
            </div>
        </div>
        <div class="item">
            <img src="/img/carousel-2.jpg" alt="Machine Innovation & Automation is the lead company in implementing INDUSTRY 4.0 in your machine tool company!">

            <div class="carousel-caption">
                <h1>{{ trans('home.title_2') }}</h1>

                <p>{{ trans('home.caption_2') }}</p>
            </div>
        </div>
        <div class="item">
            <img src="/img/carousel-3.jpg" alt="Machine Innovation & Automation is the lead company in implementing INDUSTRY 4.0 in your machine tool company!">

            <div class="carousel-caption">
                <h1>{{ trans('home.title_3') }}</h1>

                <p>{{ trans('home.caption_3') }}</p>
            </div>
        </div>
        <div class="item">
            <img src="/img/carousel-4.jpg" alt="Machine Innovation & Automation is the lead company in implementing INDUSTRY 4.0 in your machine tool company!">

            <div class="carousel-caption">
                <h1>{{ trans('home.title_4') }}</h1>

                <p>{{ trans('home.caption_4') }}</p>
            </div>
        </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#x-corp-carousel" role="button" data-slide="prev">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#x-corp-carousel" role="button" data-slide="next">
        <i class="fa fa-angle-right" aria-hidden="true"></i>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- #x-corp-carousel-->
	
@stop

@section('content')

 
<section class="x-services ptb-100 gray-bg">

    <section class="section-title">
        <div class="container text-center">
            <h2>{{ trans('home.what_we_offer') }}</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="thumbnail clearfix">
                    <img class="img-responsive" src="/img/offer1.jpg" alt="Machine Innovation & Automation is the lead company in implementing INDUSTRY 4.0 in your machine tool company!">

                    <div class="caption">
                        <h3 class="blue">Machine Innovation & Automation <br><small class="blue">{{ trans('home.section_1_sub') }}</small></h3>

                        <p>{{ trans('home.section_1_caption') }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="thumbnail clearfix">
                    <img class="img-responsive" src="/img/offer2.jpg" alt="Machine Innovation & Automation is the lead company in implementing INDUSTRY 4.0 in your machine tool company!">

                    <div class="caption">
                        <h3 class="blue">{{ trans('home.section_2_title') }}</h3>

                        <p>{{ trans('home.section_2_caption') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-md-6">
                <div class="thumbnail clearfix pb-18">
                    <img class="img-responsive" src="/img/offer3.jpg" alt="Machine Innovation & Automation is the lead company in implementing INDUSTRY 4.0 in your machine tool company!">

                    <div class="caption">
                        <h3 class="blue">{{ trans('home.section_3_title') }}<br><small class="blue">{{ trans('home.section_3_sub') }}</small></h3>

                        <p>{{ trans('home.section_3_caption') }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="thumbnail clearfix pb-18">
                    <img class="img-responsive" src="/img/offer4.jpg" alt="Machine Innovation & Automation is the lead company in implementing INDUSTRY 4.0 in your machine tool company!">

                    <div class="caption">
                        <h3 class="blue">{{ trans('home.section_4_title') }}</h3>

                        <p>{{ trans('home.section_4_caption') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- .row -->
    </div>
    <!-- .container -->
</section>
<!-- .x-services -->

<section class="x-features">
    <section class="section-title">
        <div class="container center">
            <h2>{{ trans('home.explore_machines') }}</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
              <div class="MultiCarousel" data-items="1,1,2,3" data-slide="1" id="MultiCarousel"  data-interval="1000">
                <div class="MultiCarousel-inner">
                    <div class="item active"></div>
                    @if(count($items))
                        @foreach($items as $item)
                            <div class="item">
                                <div class="col-md-12 divider">
                                    <div class="promo-block-wrapper clearfix">
                                        <a href="/product/{{ $item->id }}">
                                                <h3 class="title pt-20">{{ $item->item_title }}</h3>
                                                @if(count($item->item_photos))
                                                    @foreach($item->item_photos as $photo)
                                                        @if($loop->first)
                                                            <div class="img-wrap crop-md">
                                                                <a href="/product/{{ $item->id }}"><img class="img-responsive" src="{{ $photo->photo_path }}" alt="{{ $item->item_title }}"></a>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <h3 class="text-danger">{{ trans('items.no_photos') }}</h3>
                                                @endif
                                            
                                            <div class="row data divider-btm pb-20 center">
                                                <div class="col-sm-6 pt-10">
                                                    <span>{{ trans('items.year') }}:</span><br>
                                                    <strong>{{ $item->year }}</strong>
                                                </div>
                                                <div class="col-sm-6 pt-10 center">
                                                    <span>{{ trans('items.location') }}:</span><br>
                                                    <strong>{{ $item->location }}</strong>
                                                </div>
                                            </div>
                                        </a>
                                    </div> 
                                </div>
                            </div>
                            @endforeach
                    @else
                        <h1>{{ trans('items.not_available') }}</h1>
                    @endif
                </div>
                <a class="left carousel-control leftLst" ><i class="glyphicon glyphicon-chevron-left"></i></a>
                <a class="right carousel-control rightLst" ><i class="glyphicon glyphicon-chevron-right"></i></a>
              </div>
            </div>
        </div>
    </div>
</section>
<!-- .x-features -->



<section class="client-logo ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>{{ trans('home.brands') }}</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                <span><img src="/img/brand-1.jpg" alt="Image"></span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                <span><img src="/img/brand-2.jpg" alt="Image"></span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                <span><img src="/img/brand-3.jpg" alt="Image"></span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                <span><img src="/img/brand-4.jpg" alt="Image"></span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                <span><img src="/img/brand-5.jpg" alt="Image"></span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                <span><img src="/img/brand-6.jpg" alt="Image"></span>
            </div>
        </div>
    </div>
    <!--end of .container -->
</section>
<!-- /.client-logo -->

@stop

@section('page_script')


@stop