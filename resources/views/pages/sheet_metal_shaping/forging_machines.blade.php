@extends('layouts.master')



@section('header')

<div class="ptb-100"></div>
<!-- .page-title -->

@stop




@section('content')


<section class="ptb-100 forging-img">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <section class="section-title">
                    <h1>{{ trans('forging.forging') }}</h1>
                </section>
            </div>
            <div class="row">    
                <div class="col-md-12 transparent cat-list section-title">
                    <span class="bordered-icon ml-5"><i class="fa fa-circle-thin"></i></span>
                    <ul>
                        <li>
                            <a href="/products/48">{{ trans('forging.cold_forging') }}</a> 
                        </li>
                        <li>
                            <a href="/products/49">{{ trans('forging.forging_hammers') }}</a> 
                        </li>
                        <li>
                            <a href="/products/50">{{ trans('forging.forging_presses') }}</a> 
                        </li>
                        <li>
                            <a href="/products/51">{{ trans('forging.screw_presses') }}</a> 
                        </li>
                    </ul>
                </div>
            </div>
        <!-- /.row -->
        </div>
    </div>


</section>


<section class="service-icon-style ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>{{ trans('items.more_offers') }}</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>


    <div class="container text-center">
        <div class="row">
            <div class="container text-center">
                @if(count($items))
                    @foreach($items as $item)

                        <div class="col-sm-6 col-md-3 divider">
                            <div class="promo-block-wrapper clearfix">
                                <a href="/product/{{ $item->id }}">
                                        <h3 class="title">{{ $item->item_title }}</h3>
                                        @if(count($item->item_photos))
                                            @foreach($item->item_photos as $photo)
                                                @if($loop->first)
                                                    <div class="img-wrap crop">
                                                        <a href="/product/{{ $item->id }}"><img class="img-responsive" src="{{ $photo->photo_path }}" alt="{{ $item->item_title }}"></a>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @else
                                            <h3 class="text-danger">{{ trans('items.no_photos') }}</h3>
                                        @endif
                                    
                                    <div class="row data b-divider pb-20">
                                        <div class="col-sm-6 pt-20">
                                            <span>{{ trans('items.year') }}:</span><br>
                                            <strong>{{ $item->year }}</strong>
                                        </div>
                                        <div class="col-sm-6 pt-20">
                                            <span>{{ trans('items.location') }}:</span><br>
                                            <strong>{{ $item->location }}</strong>
                                        </div>
                                    </div>
                                </a>
                            </div> 
                        </div>

                    @endforeach
                @else
                    <h1>{{ trans('items.not_available') }}</h1>
                @endif
                    <!-- /.promo-block-wrapper -->
            </div>
        </div>
        <!-- /.row -->

    </div>


</section>
<!-- .about-text-->

@stop

@section('page_script')


@stop