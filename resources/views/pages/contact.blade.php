@extends('layouts.master')



@section('header')

<section class="contact-page-section page-title">
    <div class="container text-center">
        <h2>{{ trans('contact.header_contact') }}</h2>
    </div>
</section>
<!-- .page-title -->

@stop



@section('content')

<section class="contact-form ptb-100">
    <div class="container text-center">
        <h2>{{ trans('contact.sub_contact_1') }}<br><small>{{ trans('contact.sub_contact_2') }}</small></h2>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form method="POST" action="/contactMessage" id="form">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group user-name">
                                <label for="f_name" class="sr-only">{{ trans('contact.f_name') }}</label>
                                <input type="text" class="form-control" name="f_name" value="{{ old('f_name') }}" required placeholder="{{ trans('contact.f_name') }}">
                            </div>
                            <div class="form-group user-name">
                                <label for="l_name" class="sr-only">{{ trans('contact.l_name') }}</label>
                                <input type="text" class="form-control" name="l_name" value="{{ old('l_name') }}" required  placeholder="{{ trans('contact.l_name') }}">
                            </div>

                            <div class="form-group user-company">
                                <label for="company" class="sr-only">{{ trans('contact.company') }}</label>
                                <input type="text" class="form-control" name="company" value="{{ old('company') }}" required placeholder="{{ trans('contact.company') }}">
                            </div>

                            <div class="form-group user-email">
                                <label for="email" class="sr-only">{{ trans('contact.email') }}</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="{{ trans('contact.email') }}">
                            </div>


                            <div class="form-group user-phone">
                                <label for="phone" class="sr-only">{{ trans('contact.phone') }}</label>
                                <input type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required placeholder="{{ trans('contact.phone') }}">
                            </div>
                        </div><!-- /.col-md-6 -->
                        <div class="col-md-6">
                            <div class="form-group user-title">
                                <label for="title" class="sr-only">{{ trans('contact.title') }}</label>
                                <input type="text" class="form-control" name="title" value="{{ old('title') }}" required placeholder="{{ trans('contact.title') }}">
                            </div>
                            <div class="form-group user-message">
                                <label for="message" class="sr-only">{{ trans('contact.message') }}</label>
                                <textarea class="form-control" name="message" value="{{ old('message') }}" required placeholder="{{ trans('contact.message') }}"></textarea>
                            </div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row-->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="captcha">
                                <div class="g-recaptcha" data-sitekey="6LeSVTQUAAAAAP3ap_Bzjy239MQfX9tC9LcVqgqe"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 ptb-20">
                            <input type="submit" class="btn btn-primary contact-btn" value="{{ trans('contact.send_message') }}">
                        </div>
                    </div>
                </form>

                @if (count($errors) > 0)
                <div class="row">
                    <div class="col-md-12 col-sm-12 alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
            </div><!-- /.col-md-8 -->
        </div>

    </div>
</section>
<!-- .contact-form-->

<section class="map-section">
    <div id="googleMap"></div>
</section>
    <!--.map-section-->
      
@stop

@section('page_script')

    {{-- <script src="https://maps.googleapis.com/maps/api/js"></script> --}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARFUludLgoj21qTwiMBPgK5ntiNL9RdmY"></script>

@stop

 {{-- AIzaSyARFUludLgoj21qTwiMBPgK5ntiNL9RdmY  --}}