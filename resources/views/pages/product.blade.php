@extends('layouts.master')



@section('header')


<!-- .page-title -->

@stop




@section('content')

<section class="ptb-100 gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 white-bg">
            @if(session('locale') == 'en')    
                <h1 class="title centered ptb-90">{{ $item->category->name }}</h1>
            @elseif(session('locale') == 'sr-Latn')
                <h1 class="title centered ptb-90">{{ $item->category->name_rs }}</h1>
            @endif
            <h2 class="title centered pb-20">{{ $item->item_title }}</h2>

                <div class="col-md-6 table-data">
                    <table>
                        <tbody>
                            <tr>
                                <th>{{ trans('items.manufacturer') }}</th>
                                <td>{{ $item->manufacturer }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('items.model') }}</th>
                                <td>{{ $item->model }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('items.year') }}</th>
                                <td>{{ $item->year }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('items.location') }}</th>
                                <td>{{ $item->location }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('items.category') }}</th>
                                @if(session('locale') == 'en')    
                                    <td>{{ $item->category->name }}</td>
                                @elseif(session('locale') == 'sr-Latn')
                                    <td>{{ $item->category->name_rs }}</td>
                                @endif
                            </tr>
                            <tr>
                                <th>{{ trans('items.id') }}</th>
                                <td>MIA-{{ $item->id }}</td>
                            </tr>
                            <tr>
                                <th>{{ trans('items.price') }}</th>
                                <td>{{ $item->price }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="ptb-50">
                        <div>
                            <a href="{{ route('page.contact_seller', ['item_id' => $item->id, 'trans' => null]) }}" class="btn btn-primary contact-btn">{{ trans('product.contact_seller') }}</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 pt-20">
                    <div id="slider" class="flexslider">
                      <ul class="slides">
                        @if(count($item->item_photos))
                            @foreach($item->item_photos as $photo)
                                <li>
                                  <img class="img-responsive" src="{{ $photo->photo_path }}" alt="{{ $item->item_title }}" />
                                </li>
                            @endforeach
                        @else
                            <h3 class="text-danger">{{ trans('items.no_photos') }}</h3>
                        @endif
                      </ul>
                    </div>
                    <div id="carousel" class="flexslider">
                      <ul class="slides">
                        @if(count($item->item_photos))
                            @foreach($item->item_photos as $photo)
                                <li class="crop">
                                  <img class="img-responsive" src="{{ $photo->photo_path }}" alt="{{ $item->item_title }}" />
                                </li>
                            @endforeach
                        @else
                            <h3 class="text-danger">{{ trans('items.no_photos') }}</h3>
                        @endif
                        <!-- items mirrored twice, total of 12 -->
                      </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 trans">
                <div class="col-md-12">
                    <h1>{{ trans('product.trans_title_1') }} <span>{{ trans('product.trans_title_2') }}</span> {{ trans('product.trans_title_3') }}</h1>
                    <h3>{{ trans('product.trans_1') }}<br>{{ trans('product.trans_2') }}</h3>
                    <p>{{ trans('product.trans_3') }}</p>
                </div>
                <div class="col-md-3 col-sm-12 mt-20">
                    <a class="crop-sm" href="{{ route('page.contact_seller', ['item_id' => $item->id, 'trans' => 1]) }}"><img src="/img/trans_1.jpg" alt="Transportation"></a>
                </div>
                <div class="col-md-3 col-sm-12 mt-20">
                    <a class="crop-sm" href="{{ route('page.contact_seller', ['item_id' => $item->id, 'trans' => 2]) }}"><img src="/img/trans_2.jpg" alt="Transportation"></a>
                </div>
                <div class="col-md-3 col-sm-12 mt-20">
                    <a class="crop-sm" href="{{ route('page.contact_seller', ['item_id' => $item->id, 'trans' => 3]) }}"><img src="/img/trans_3.jpg" alt="Transportation"></a>
                </div>
                <div class="col-md-3 col-sm-12 mt-20">
                    <a class="crop-sm" href="{{ route('page.contact_seller', ['item_id' => $item->id, 'trans' => 4]) }}"><img src="/img/trans_4.jpg" alt="Transportation"></a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-12 white-bg">
                <div class="col-md-6 specifications">
                    <h1 class="title">{{ trans('items.specifications') }}</h1>
                    <table>
                        <tbody>
                            @foreach($item_specs as $key => $val)
                                <tr>
                                    <th>{{ $key }}</th>
                                    <td>{{ $val }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6 pt-20">
                    <h1 class="title">{{ trans('items.description') }}</h1>
                    <div class="description">
                        <p>{{ $item->description }}</p>
                    </div>
                </div>
                    
        </div>

    </div>


    <div class="row">
        <div class="ptb-50 centerd">
            <a href="{{ route('page.contact_seller', ['item_id' => $item->id, 'trans' => null]) }}" class="btn btn-primary contact-btn">{{ trans('product.contact_seller') }}</a>
        </div>  
    </div>


</section>
<!-- product-->


<section class="service-icon-style ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>{{ trans('items.more_offers') }}</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>


    <div class="container text-center">
        <div class="row">
            <div class="container text-center">
                @if(count($category_items))
                    @foreach($category_items as $item)

                        <div class="col-sm-6 col-md-3 divider">
                            <div class="promo-block-wrapper clearfix">
                                <a href="/product/{{ $item->id }}">
                                        <h3 class="title">{{ $item->item_title }}</h3>
                                        @if(count($item->item_photos))
                                            @foreach($item->item_photos as $photo)
                                                @if($loop->first)
                                                    <div class="img-wrap crop">
                                                        <a href="/product/{{ $item->id }}"><img class="img-responsive" src="{{ $photo->photo_path }}" alt="{{ $item->item_title }}"></a>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @else
                                            <h3 class="text-danger">{{ trans('items.no_photos') }}</h3>
                                        @endif
                                    
                                    <div class="row data b-divider pb-20">
                                        <div class="col-sm-6 pt-20">
                                            <span>{{ trans('items.year') }}:</span><br>
                                            <strong>{{ $item->year }}</strong>
                                        </div>
                                        <div class="col-sm-6 pt-20">
                                            <span>{{ trans('items.location') }}:</span><br>
                                            <strong>{{ $item->location }}</strong>
                                        </div>
                                    </div>
                                </a>
                            </div> 
                        </div>

                    @endforeach
                @else
                    <h1>{{ trans('items.not_available') }}</h1>
                @endif
                    <!-- /.promo-block-wrapper -->
            </div>
        </div>
        <!-- /.row -->

    </div>


</section>
<!-- .about-text-->

@stop

@section('page_script')


    <script type="text/javascript">
        $(window).load(function() {
          $('#carousel').flexslider({
            animation: "slide",
            controlNav: true,
            animationLoop: false,
            slideshow: false,
            itemWidth: 180,
            itemMargin: 5,
            asNavFor: '#slider'
          });
         
          $('#slider').flexslider({
            animation: "slide",
            controlNav: true,
            animationLoop: false,
            slideshow: false,
            directionNav: false,
            sync: "#carousel"
          });
        });
    </script>
@stop