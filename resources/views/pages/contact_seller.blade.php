@extends('layouts.master')



@section('header')

<section class="sell-page-section page-title">
    <div class="container text-center">
        <h2>{{ trans('contact.header_sell') }}</h2>
    </div>
</section>
<!-- .page-title -->

@stop





@section('content')

<section class="contact-form ptb-100">
    <div class="container text-center">
        <h2>{{ trans('contact.sub_sell_1') }}<br>{{ trans('contact.sub_sell_2') }}</h2>

        <div class="row">
            {{-- form --}}
            <div class="col-md-6 col-sm-12">
                <form method="POST" action="/sellerMessage" id="form" class="form-group">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group user-name">
                                <label for="f_name" class="sr-only">{{ trans('contact.f_name') }}</label>
                                <input type="text" class="form-control" name="f_name" value="{{ old('f_name') }}" required placeholder="{{ trans('contact.f_name') }}">
                            </div>
                            <div class="form-group user-name">
                                <label for="l_name" class="sr-only">{{ trans('contact.l_name') }}</label>
                                <input type="text" class="form-control" name="l_name" value="{{ old('l_name') }}" required  placeholder="{{ trans('contact.l_name') }}">
                            </div>

                            <div class="form-group user-company">
                                <label for="company" class="sr-only">{{ trans('contact.company') }}</label>
                                <input type="text" class="form-control" name="company" value="{{ old('company') }}" required placeholder="{{ trans('contact.company') }}">
                            </div>

                            <div class="form-group user-email">
                                <label for="email" class="sr-only">{{ trans('contact.email') }}</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="{{ trans('contact.email') }}">
                            </div>
                            <div class="form-group user-phone">
                                <label for="phone" class="sr-only">{{ trans('contact.phone') }}</label>
                                <input type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required placeholder="{{ trans('contact.phone') }}">
                            </div>
                            <div class="form-group user-title">
                                <label for="title" class="sr-only">{{ trans('contact.title') }}</label>
                                <input type="text" class="form-control" name="title" value="{{ old('title') }}" required placeholder="{{ trans('contact.title') }}">
                            </div>
                            <div class="form-group user-message text-lg">
                                <label for="message" class="sr-only">{{ trans('contact.message') }}</label>
                                <textarea class="form-control" name="message" value="{{ old('message') }}" required placeholder="{{ trans('contact.message') }}"></textarea>
                            </div>
                            <input type="text" class="form-control hidden" name="item_id" value="{{ $item->id }}" required hidden>
                            <input type="text" class="form-control hidden" name="trans_src" value="{{ $trans_src }}" hidden>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row-->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="captcha">
                                <div class="g-recaptcha" data-sitekey="6LeSVTQUAAAAAP3ap_Bzjy239MQfX9tC9LcVqgqe"></div>
                            </div>
                        </div>
                    </div>
                    
                </form>
            </div><!-- /form -->

            <div class="col-md-6 col-sm-12">
                
            @if(count($item))

                {{-- Selected product --}}
                <div class="col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="title-fix">{{ trans('contact.selected_machine') }}</span></div>

                        <div class="promo-block-wrapper clearfix">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <h3 class="blue pt-10">{{ $item->item_title }}</h3>
                                    @if(count($item->item_photos))
                                        @foreach($item->item_photos as $photo)
                                            @if($loop->first)
                                                <div class="img-wrap crop">
                                                    <a href="/product/{{ $item->id }}"><img class="img-responsive" src="{{ $photo->photo_path }}" alt="{{ $item->item_title }}"></a>
                                                </div>
                                            @endif
                                        @endforeach
                                    @else
                                        <h3 class="text-danger">{{ trans('items.no_photos') }}</h3>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 data">
                                    <div class="table-cat-sm">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <th>{{ trans('items.year') }}</th>
                                                    <td>{{ $item->year }}</td>
                                                </tr>
                                                <tr>
                                                    <th>{{ trans('items.location') }}</th>
                                                    <td>{{ $item->location }}</td>
                                                </tr>
                                                <tr>
                                                    <th>{{ trans('items.price') }}</th>
                                                    <td>{{ $item->price }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {{-- End selected product --}}
                @else
                    <h1>{{ trans('contact.error') }}</h1>
                @endif

                @if(!is_null($trans_src))
                 <div class="col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="title-fix">{{ trans('contact.selected_trans') }}</span></div>
                        <p> {{ trans('contact.notice_1') }}<br>
                         {{ trans('contact.notice_2') }}<br>
                        </p>
                        <div class="crop-sm">
                            <img src="{{ $trans_src }}">
                        </div>
                        <small class="text-danger">{{ trans('contact.notice_3') }}</small>
                    </div>
                </div>
                @endif

            </div>

            @if (count($errors) > 0)
            <div class="row">
                <div class="col-md-12 col-sm-12 alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ptb-20">
                <input type="submit" class="btn btn-primary btn-lg" form="form" value="{{ trans('contact.send_message') }}">
            </div>
        </div>
    </div>
</section>
<!-- .contact-form-->

      
@stop

@section('page_script')


@stop