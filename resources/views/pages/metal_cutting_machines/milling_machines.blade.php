@extends('layouts.master')



@section('header')

<div class="ptb-100"></div>
<!-- .page-title -->

@stop




@section('content')


<section class="ptb-100 milling-img">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <section class="section-title">
                    <h1>{{ trans('milling.milling') }}</h1>
                </section>
            </div>
            <div class="row">    
                <div class="col-md-12 transparent cat-list section-title">
                    <span class="bordered-icon ml-5"><i class="fa fa-circle-thin"></i></span>
                    <ul>
                        <li>
                            <a href="/products/34">{{ trans('milling.horizontal_miling') }}</a> 
                        </li>
                        <li>
                            <a href="/products/35">{{ trans('milling.horizontal_cnc') }}</a> 
                        </li>
                        <li>
                            <a href="/products/36">{{ trans('milling.universal_milling') }}</a> 
                        </li>
                        <li>
                            <a href="/products/37">{{ trans('milling.universal_cnc') }}</a> 
                        </li>
                        <li>
                            <a href="/products/38">{{ trans('milling.portal_milling') }}</a> 
                        </li>
                        <li>
                            <a href="/products/39">{{ trans('milling.vertical_milling') }}</a> 
                        </li>
                        <li>
                            <a href="/products/40">{{ trans('milling.vertical_cnc') }}</a> 
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>


</section>


<section class="service-icon-style ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>Special offers in this category!</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>


    <div class="container text-center">
        <div class="row">
            <div class="container text-center">
                @if(count($items))
                    @foreach($items as $item)

                        <div class="col-sm-6 col-md-3 divider">
                            <div class="promo-block-wrapper clearfix">
                                <a href="/product/{{ $item->id }}">
                                        <h3 class="title">{{ $item->item_title }}</h3>
                                        @if(count($item->item_photos))
                                            @foreach($item->item_photos as $photo)
                                                @if($loop->first)
                                                    <div class="img-wrap crop">
                                                        <a href="/product/{{ $item->id }}"><img class="img-responsive" src="{{ $photo->photo_path }}" alt="{{ $item->item_title }}"></a>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @else
                                            <h3 class="text-danger">No Photos!</h3>
                                        @endif
                                    
                                    <div class="row data b-divider pb-20">
                                        <div class="col-sm-6 pt-20">
                                            <span>YEAR:</span><br>
                                            <strong>{{ $item->year }}</strong>
                                        </div>
                                        <div class="col-sm-6 pt-20">
                                            <span>LOCATION:</span><br>
                                            <strong>{{ $item->location }}</strong>
                                        </div>
                                    </div>
                                </a>
                            </div> 
                        </div>

                    @endforeach
                @else
                    <h1>No available items at the moment...</h1>
                @endif
                    <!-- /.promo-block-wrapper -->
            </div>
        </div>
        <!-- /.row -->

    </div>


</section>
<!-- .about-text-->

@stop

@section('page_script')


@stop