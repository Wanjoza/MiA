@extends('layouts.master')



@section('header')

<section class="about-page-section page-title">
    <div class="container text-center">
        <h2>{{ trans('about.about') }}</h2>
    </div>
</section>
<!-- .page-title -->

@stop





@section('content')

 <section class="about-text pt-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>{{ trans('about.title') }}</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-12 about">
                <p>{{ trans('about.intro') }} <span class="blue">24/7</span> {{ trans('about.intro_1') }} 
                </p><br>
                <p>
                    <span class="fn-md"> {{ trans('about.content_1') }} <span class="blue">{{ trans('about.content_2') }}</span> {{ trans('about.content_3') }}</span><br><br>
                    <b class="blue">-</b>   {{ trans('about.content_4') }} <b class="blue">-</b><br>
                    <b class="blue">-</b>   {{ trans('about.content_5') }} <b class="blue">-</b><br>
                    <b class="blue">-</b>   {{ trans('about.content_6') }} <b class="blue">-</b><br>
                    <b class="blue">-</b>   {{ trans('about.content_7') }} <b class="blue">-</b><br>
                </p>
                <hr>
                <p>
                    <span class="fn-md"> {{ trans('about.content_1') }}  <span class="blue">{{ trans('about.content_8') }} </span> {{ trans('about.content_3') }} </span><br><br>
                    <b class="blue">-</b>   {{ trans('about.content_9') }} <b class="blue">-</b><br>
                    <b class="blue">-</b>   {{ trans('about.content_10') }} <b class="blue">-</b><br>
                    <b class="blue">-</b>   {{ trans('about.content_11') }}  <b class="blue">-</b><br>
                    <b class="blue">-</b>   {{ trans('about.content_12') }} <b class="blue">-</b><br>
                    <b class="blue">-</b>   {{ trans('about.content_13') }}   <b class="blue">-</b><br>
                    <b class="blue">-</b>   {{ trans('about.content_14') }}   <b class="blue">-</b><br>
                    <b class="blue">-</b>   {{ trans('about.content_15') }}   <b class="blue">-</b><br>
                    <b class="blue">-</b>   {{ trans('about.content_16') }}  <b class="blue">-</b><br>
                </p>
                <hr>
                <p>{{ trans('about.content_17') }}<br>
                    <span class="blue fn-m">{{ trans('about.content_18') }}</span> 
                </p><br>
                <p>
                    {{ trans('about.content_19') }}<br>
                    {{ trans('about.content_20') }} <a href="https://www.techpilot.net/" target="_blank" class="blue fn-md">Techpilot</a>.<br>
                    {{ trans('about.content_21') }}<br>
                    {{ trans('about.content_22') }}
                    
                </p><br>
                <p>
                    {{ trans('about.outro_1') }}<span class="fn-m blue">4.0</span> {{ trans('about.outro_2') }}<br>
                    <a href="contact" class="blue fn-md">{{ trans('about.outro_3') }}</a> {{ trans('about.outro_4') }}
                    <br><br><br>

                    <span class="blue fn-lg mt-50">{{ trans('about.outro_5') }}</span>
                </p><hr>
            </div>
        </div>
    </div>

</section>
<!-- .about-text-->





<section class="client-logo ptb-20">
    <section class="section-title">
        <div class="container text-center">
            <h2>{{ trans('about.outro_6') }}</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                <span><img src="/img/brand-1.jpg" alt="Image"></span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                <span><img src="/img/brand-2.jpg" alt="Image"></span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                <span><img src="/img/brand-3.jpg" alt="Image"></span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                <span><img src="/img/brand-4.jpg" alt="Image"></span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                <span><img src="/img/brand-5.jpg" alt="Image"></span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 section-margin">
                <span><img src="/img/brand-6.jpg" alt="Image"></span>
            </div>
        </div>
    </div>
    <!--end of .container -->
</section>
<!-- /.client-logo -->

      
@stop

@section('page_script')


@stop