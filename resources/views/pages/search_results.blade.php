@extends('layouts.master')



@section('header')

<section class="products-page-section page-title">
    <div class="container text-center">
        
    </div>
</section>
<!-- .page-title -->

@stop




@section('content')


<section class="service-icon-style ptb-100">
    <section class="section-title">
        <div class="container text-center">
            @if(strlen($term) >= 3)
                <h2>{{ trans('search.result') }}: "<i class="blue"> {{ $term }} </i>"</h2>
                @if($items->total() > 1)
                    <h4>{{ trans('search.total') }} <span class="blue">{{ $items->total() }} </span> {{ trans('search.items') }}</h4>
                @elseif($items->total() == 1)
                    <h4>{{ trans('search.only') }} <span class="blue"> {{ $items->total() }} </span> {{ trans('search.item') }}</h4>
                @else
                    <h4 class="blue">{{ trans('search.mistype') }}</h4>
                @endif
            @else
                <h2 class="text-danger">{{ trans('search.min') }}</h2>
            @endif
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    @if(strlen($term) >= 3)

        <div class="container text-center">

            @if(count($items))
                @foreach($items as $item)
                    {{-- Single product --}}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="promo-block-wrapper clearfix">

                                    @if(count($item->item_photos))
                                        @foreach($item->item_photos as $photo)
                                            @if($loop->first)
                                                <div class="img-wrap col-md-5 crop-lg">
                                                    <a href="/product/{{ $item->id }}"><img class="img-responsive" src="{{ $photo->photo_path }}" alt="{{ $item->item_title }}"></a>
                                                </div>
                                            @endif
                                        @endforeach
                                    @else
                                        <h3 class="text-danger">{{ trans('items.no_photos') }}</h3>
                                    @endif

                                    <div class="col-md-6 col-sm-6 data pb-20">
                                        <div class="table-cat">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <a href="/product/{{ $item->id }}"><h2 class="title">{{ $item->item_title }}</h2></a>
                                                    </tr>
                                                    <tr>
                                                        <th>{{ trans('items.year') }}</th>
                                                        <td>{{ $item->year }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>{{ trans('items.location') }}</th>
                                                        <td>{{ $item->location }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>{{ trans('items.price') }}</th>
                                                        <td>{{ $item->price }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </div>
                    {{-- End single product --}}
                @endforeach
            @else
                        <h1>{{ trans('items.not_available') }}</h1>
            @endif

            {{-- Pagination --}}
            <div class="row">
                <div class="col-md-12 col-sm-12 center">{{ $items->links() }}</div>
            </div>

        </div>    
    @endif

</section>
<!-- .about-text-->

@stop

@section('page_script')


@stop