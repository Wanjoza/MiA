@extends('layouts.master')



@section('header')

<div class="ptb-100"></div>
<!-- .page-title -->

@stop




@section('content')


<section class="cat-img bck-img">
    <div class="container">
        <div class="row">
            <h1 class="centerd pb-40">{{ trans('buy.categories') }}</h1>

            {{-- Category 1 --}}

            <div class="col-md-3 col-sm-12 x-accordion">
                <div class="panel-group" id="l-categories">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#l-categories" href="#collapseCat1" aria-expanded="false" class="collapsed">
                                    {{ trans('navbar.cat_1_1') }} <br> {{ trans('navbar.cat_1_2') }}
                                </a>
                            </h4>
                        </div>
                        <div id="collapseCat1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                {{-- Category 1 submenu 1 --}}
                                <div class="panel-group" id="cat1Scnd1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#cat1Scnd1" href="#collapseCat1Scnd1" aria-expanded="false" class="collapsed">{{ trans('borers.borers') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div id="collapseCat1Scnd1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <a class="panel-title" href="/products/26"> {{ trans('borers.deep_hole_boring') }}</a><br><br>
                                            <a class="panel-title" href="/products/27"> {{ trans('borers.floor_type') }}</a><br><br>
                                            <a class="panel-title" href="/products/28"> {{ trans('borers.floor_cnc') }}</a><br><br>
                                            <a class="panel-title" href="/products/29"> {{ trans('borers.table_type') }}</a><br><br>
                                            <a class="panel-title" href="/products/30"> {{ trans('borers.table_cnc') }}</a><br><br>
                                        </div>
                                    </div>
                                </div>
                                {{-- Category 1 submenu 1 END --}}

                                {{-- Category 1 submenu 2 --}}
                                <div class="panel-group" id="cat1Scnd2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#cat1Scnd2" href="#collapseCat1Scnd2" aria-expanded="false" class="collapsed">{{ trans('grinders.grinders') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div id="collapseCat1Scnd2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <a class="panel-title" href="/products/31"> {{ trans('grinders.cylindrical_centrless') }}</a><br><br>
                                            <a class="panel-title" href="/products/32"> {{ trans('grinders.cylindrical_ext') }}</a><br><br>
                                            <a class="panel-title" href="/products/33"> {{ trans('grinders.surface_grinders') }}</a><br><br>
                                        </div>
                                    </div>
                                </div>
                                {{-- Category 1 submenu 2 END --}}

                                {{-- Category 1 submenu 3 --}}
                                <div class="panel-group" id="cat1Scnd3">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#cat1Scnd3" href="#collapseCat1Scnd3" aria-expanded="false" class="collapsed">{{ trans('milling.milling') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div id="collapseCat1Scnd3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <a class="panel-title" href="/products/34"> {{ trans('milling.horizontal_miling') }}</a><br><br>
                                            <a class="panel-title" href="/products/35"> {{ trans('milling.horizontal_cnc') }}</a><br><br>
                                            <a class="panel-title" href="/products/36"> {{ trans('milling.universal_milling') }}</a><br><br>
                                            <a class="panel-title" href="/products/37"> {{ trans('milling.universal_cnc') }}</a><br><br>
                                            <a class="panel-title" href="/products/38"> {{ trans('milling.portal_milling') }}</a><br><br>
                                            <a class="panel-title" href="/products/39"> {{ trans('milling.vertical_milling') }}</a><br><br>
                                            <a class="panel-title" href="/products/40"> {{ trans('milling.vertical_cnc') }}</a><br><br>
                                        </div>
                                    </div>
                                </div>
                                {{-- Category 1 submenu 3 END --}}

                                {{-- Category 1 submenu 4 --}}
                                <div class="panel-group" id="cat1Scnd4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#cat1Scnd4" href="#collapseCat1Scnd4" aria-expanded="false" class="collapsed">{{ trans('machininng_centers.machininng_centers') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div id="collapseCat1Scnd4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <a class="panel-title" href="/products/41"> {{ trans('machininng_centers.5_axe') }}</a><br><br>
                                            <a class="panel-title" href="/products/42"> {{ trans('machininng_centers.horizontal') }}</a><br><br>
                                            <a class="panel-title" href="/products/43"> {{ trans('machininng_centers.vertical') }}</a><br><br>
                                        </div>
                                    </div>
                                </div>

                                {{-- Category 1 submenu 4 END --}}

                                {{-- Category 1 submenu 5 --}}
                                <div class="panel-group" id="cat1Scnd5">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#cat1Scnd5" href="#collapseCat1Scnd5" aria-expanded="false" class="collapsed">{{ trans('lathes.lathes') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div id="collapseCat1Scnd5" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <a class="panel-title" href="/products/44"> {{ trans('lathes.vertical_turret') }}</a><br><br>
                                            <a class="panel-title" href="/products/45"> {{ trans('lathes.horizintal_turret') }}</a><br><br>
                                            <a class="panel-title" href="/products/46"> {{ trans('lathes.automatic_lathes') }}</a><br><br>
                                            <a class="panel-title" href="/products/47"> {{ trans('lathes.swiss_type') }}</a><br><br>
                                        </div>
                                    </div>
                                </div>

                                {{-- Category 1 submenu 5 END --}}

                                {{-- Category 1 submenu 6 --}}
                                <div class="panel-group" id="cat1Scnd6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="/products/10">{{ trans('navbar.bar_loaders') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>

                                {{-- Category 1 submenu 6 END --}}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Category 1 END -->


            {{-- Category 2 --}}

            <div class="col-md-3 col-sm-12 x-accordion">
                <div class="panel-group" id="lc-categories">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#lc-categories" href="#collapseCat2" aria-expanded="false" class="collapsed">
                                    {{ trans('navbar.cat_2_1') }}  <br> {{ trans('navbar.cat_2_2') }}
                                </a>
                            </h4>
                        </div>
                        <div id="collapseCat2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                {{-- Category 2 submenu 1 --}}
                                <div class="panel-group" id="cat2Scnd1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#cat2Scnd1" href="#collapseCat2Scnd1" aria-expanded="false" class="collapsed">{{ trans('forging.forging') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div id="collapseCat2Scnd1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <a class="panel-title" href="/products/48"> {{ trans('forging.cold_forging') }}</a><br><br>
                                            <a class="panel-title" href="/products/49"> {{ trans('forging.forging_hammers') }}</a><br><br>
                                            <a class="panel-title" href="/products/50"> {{ trans('forging.forging_presses') }}</a><br><br>
                                            <a class="panel-title" href="/products/51"> {{ trans('forging.screw_presses') }}</a><br><br>
                                        </div>
                                    </div>
                                </div>
                                {{-- Category 2 submenu 1 END --}}

                                {{-- Category 2 submenu 2 --}}
                                <div class="panel-group" id="cat2Scnd2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#cat2Scnd2" href="#collapseCat2Scnd2" aria-expanded="false" class="collapsed">{{ trans('presses.presses') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div id="collapseCat2Scnd2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <a class="panel-title" href="/products/52"> {{ trans('presses.eccentric') }}</a><br><br>
                                            <a class="panel-title" href="/products/53"> {{ trans('presses.hydraulic_mechanic') }}</a><br><br>
                                            <a class="panel-title" href="/products/54"> {{ trans('presses.stamping') }}</a><br><br>
                                            <a class="panel-title" href="/products/55"> {{ trans('presses.press_brake') }}</a><br><br>
                                            <a class="panel-title" href="/products/56"> {{ trans('presses.press_brake_cnc') }}</a><br><br>
                                            <a class="panel-title" href="/products/57"> {{ trans('presses.punching') }}</a><br><br>
                                            <a class="panel-title" href="/products/58"> {{ trans('presses.punching_cnc') }}</a><br><br>
                                            <a class="panel-title" href="/products/59"> {{ trans('presses.shears') }}</a><br><br>
                                            <a class="panel-title" href="/products/60"> {{ trans('presses.shears_hydraulic') }}</a><br><br>
                                            <a class="panel-title" href="/products/61"> {{ trans('presses.shears_mechanic') }}</a><br><br>
                                        </div>
                                    </div>
                                </div>
                                {{-- Category 2 submenu 2 END --}}

                                {{-- Category 2 submenu 3 --}}
                                <div class="panel-group" id="cat2Scnd3">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="/products/13">{{ trans('navbar.laser') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                {{-- Category 2 submenu 3 END --}}

                                {{-- Category 2 submenu 4 --}}
                                <div class="panel-group" id="cat2Scnd4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="/products/14">{{ trans('navbar.plasma') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>

                                {{-- Category 2 submenu 4 END --}}

                                {{-- Category 2 submenu 5 --}}
                                <div class="panel-group" id="cat2Scnd5">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="/products/15">{{ trans('navbar.waterjet') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>

                                {{-- Category 2 submenu 5 END --}}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Category 2 END -->


            {{-- Category 3 --}}

            <div class="col-md-3 col-sm-12 x-accordion">
                <div class="panel-group" id="rc-categories">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#rc-categories" href="#collapseCat3" aria-expanded="false" class="collapsed">
                                    {{ trans('navbar.cat_3_1') }} <br> {{ trans('navbar.cat_3_2') }}
                                </a>
                            </h4>
                        </div>
                        <div id="collapseCat3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">

                                {{-- Category 3 submenu 1 --}}
                                
                                <div class="panel-group" id="cat3Scnd1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="/products/16">{{ trans('navbar.blow_molding') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                {{-- Category 3 submenu 1 END --}}

                                {{-- Category 3 submenu 2 --}}
                                <div class="panel-group" id="cat3Scnd2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="/products/17">{{ trans('navbar.single_extruder') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>

                                
                                {{-- Category 3 submenu 2 END --}}


                                {{-- Category 3 submenu 3 --}}
                                <div class="panel-group" id="cat3Scnd3">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="/products/18">{{ trans('navbar.twin_extruder') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                {{-- Category 3 submenu 3 END --}}

                                {{-- Category 3 submenu 4 --}}
                                <div class="panel-group" id="cat3Scnd4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="/products/19">{{ trans('navbar.injestion_molding') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>

                                {{-- Category 3 submenu 4 END --}}

                                {{-- Category 3 submenu 5 --}}
                                <div class="panel-group" id="cat3Scnd5">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="/products/20">{{ trans('navbar.plastic_other') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>

                                {{-- Category 3 submenu 5 END --}}

                                {{-- Category 3 submenu 6 --}}
                                <div class="panel-group" id="cat3Scnd6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="/products/21">{{ trans('navbar.recycling') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>

                                {{-- Category 3 submenu 5 END --}}


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Category 3 END -->

            {{-- Category 4 --}}
            <div class="col-md-3 col-sm-12 x-accordion">
                <div class="panel-group" id="r-categories">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#r-categories" href="#collapseCat4" aria-expanded="false" class="collapsed">
                                    {{ trans('navbar.cat_4_1') }} <br> {{ trans('navbar.cat_4_2') }}
                                </a>
                            </h4>
                        </div>
                        <div id="collapseCat4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">

                                {{-- Category 4 submenu 1 --}}
                                
                                <div class="panel-group" id="cat4Scnd1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="/products/22">{{ trans('navbar.compressors') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                {{-- Category 4 submenu 1 END --}}

                                {{-- Category 4 submenu 2 --}}
                                <div class="panel-group" id="cat4Scnd2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="/products/23">{{ trans('navbar.forklifts') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>

                                
                                {{-- Category 4 submenu 2 END --}}


                                {{-- Category 4 submenu 3 --}}
                                <div class="panel-group" id="cat4Scnd3">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="/products/24">{{ trans('navbar.cranes') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                {{-- Category 4 submenu 3 END --}}

                                {{-- Category 4 submenu 4 --}}
                                <div class="panel-group" id="cat4Scnd4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#cat4Scnd4" href="#collapseCat4Scnd4" aria-expanded="false" class="collapsed">{{ trans('robots.robots') }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div id="collapseCat4Scnd4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <a class="panel-title" href="/products/62"> {{ trans('robots.abb') }}</a><br><br>
                                            <a class="panel-title" href="/products/63"> {{ trans('robots.fanuc') }}</a><br><br>
                                            <a class="panel-title" href="/products/64"> {{ trans('robots.kuka') }}</a><br><br>
                                            <a class="panel-title" href="/products/65"> {{ trans('robots.others') }}</a><br><br>
                                        </div>
                                    </div>
                                </div>

                                {{-- Category 4 submenu 4 END --}}


                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!-- Category 4 END -->

        </div>
        <!-- /.row -->

    </div>


</section>


<section class="service-icon-style ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>{{ trans('buy.solutions') }}</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>


    <div class="container text-center">
        <div class="row">
            <div class="container text-center">
                @if(count($items))
                    @foreach($items as $item)

                        <div class="col-sm-6 col-md-3 divider">
                            <div class="promo-block-wrapper clearfix">
                                <a href="/product/{{ $item->id }}">
                                        <h3 class="title">{{ $item->item_title }}</h3>
                                        @if(count($item->item_photos))
                                            @foreach($item->item_photos as $photo)
                                                @if($loop->first)
                                                    <div class="img-wrap crop">
                                                        <a href="/product/{{ $item->id }}"><img class="img-responsive" src="{{ $photo->photo_path }}" alt="{{ $item->item_title }}"></a>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @else
                                            <h3 class="text-danger">{{ trans('items.no_photos') }}</h3>
                                        @endif
                                    
                                    <div class="row data b-divider pb-20">
                                        <div class="col-sm-6 pt-20">
                                            <span>{{ trans('items.year') }}:</span><br>
                                            <strong>{{ $item->year }}</strong>
                                        </div>
                                        <div class="col-sm-6 pt-20">
                                            <span>{{ trans('items.location') }}:</span><br>
                                            <strong>{{ $item->location }}</strong>
                                        </div>
                                    </div>
                                </a>
                            </div> 
                        </div>

                    @endforeach
                @else
                    <h1>{{ trans('items.not_available') }}</h1>
                @endif
                    <!-- /.promo-block-wrapper -->
            </div>
            
        </div>
        <!-- /.row -->

    </div>


</section>
<!-- .about-text-->

@stop

@section('page_script')


@stop