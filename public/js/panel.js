    

  function populate(s1,s2){
      var s1 = document.getElementById(s1);
      var s2 = document.getElementById(s2);
      s2.innerHTML = "";
      switch(s1.value){
        case '1' :
          var optionArray = ["|--Select--","5|BORERS","6|GRINDING MACHINES","7|MILLING MACHINES","8|MACHINING CENTERS","9|LATHES","10|BAR LOADERS"];
        break;
        case '2' :
          var optionArray = ["|--Select--","11|FORGING MACHINES","12|PRESSES","13|LASER CUTTING MACHINES","14|PLASMA / GAS CUTTING MACHINES","15|WATERJET CUTTING MACHINES"];
        break;
        case '3' :
          var optionArray = ["|--Select--","16|BLOW MOLDING MACHINES","17|EXTRUSION - SINGLE SCREW EXTRUDERS","18|EXTRUSION – TWIN SCREW EXTRUDERS","19|INJECTION MOLDING MACHINES","20|PLASTIC MACHINES – OTHER","21|PLASTIC COMPACTORS / RECYCLING MACHINES"];
        break;
        case '4' :
          var optionArray = ["|--Select--","22|COMPRESORS","23|FORKLIFT","24|CRANES","25|ROBOTS"];
        break;
        case '5' :
          var optionArray = ["|--Select--","26|DEEP HOLE BORING/ DRILLING MACHINES","27|FLOOR TYPE BORING MACHINES","28|FLOOR TYPE CNC BORING MACHINES","29|TABLE TYPE BORING MACHINES","30|TABLE TYPE CNC BORING MACHINES"];
        break;
        case '6' :
          var optionArray = ["|--Select--","31|CYLINDRICAL CENTERLESS GRINDERS","32|CYLINDRICAL EXTERNAL / INTERNAL GRINDERS","33|SURFACE GRINDERS"];
        break;
        case '7' :
          var optionArray = ["|--Select--","34|HORIZONTAL MILLING MACHINES","35|HORIZONTAL CNC MILLING MACHINES","36|UNIVERSAL MILLING MACHINES","37|UNIVERSAL CNC MILLING MACHINES","38|PORTAL MILLING MACHINES","39|VERTICAL MILLING MACHINES","40|VERTICAL CNC MILLING MACHINES"];
        break;
        case '8' :
          var optionArray = ["|--Select--","41|5 AXE MACHINES","42|HORIZONTAL","43|VERTICAL"];
        break;
        case '9' :
          var optionArray = ["|--Select--","44|VERTICAL TURRET LATHE","45|HORIZONTAL TURRET LATHE","46|MULTISPINDLE AUTOMATIC LATHES","47|SWISS TYPE LATHE"];
        break;
        case '10' :
          var optionArray = ["|--Select--","10|BAR LOADERS"];
        break;
        case '11' :
          var optionArray = ["|--Select--","48|COLD FORGING MACHINES","49|FORGING HAMMERS","50|FORGING PRESSES","51|SCREW PRESSES"];
        break;
        case '12' :
          var optionArray = ["|--Select--","52|ECCENTRIC PRESSES","53|HYDRAULIC / MECHANIC PRESSES","54|STAMPING PRESSES","55|PRESS BRAKE","56|PRESS BRAKE NC /CNC","57|PUNCHING MACHINES","58|PUNCHING MACHINES CNC","59|SHEARS / GUILLOTINE","60|SHEARS / GUILLOTINE HYDRAULIC","61|SHEARS / GUILLOTINE MECHANIC"];
        break;
        case '13' :
          var optionArray = ["|--Select--","13|LASER CUTTING MACHINES"];
        break;
        case '14' :
          var optionArray = ["|--Select--","14|PLASMA / GAS CUTTING MACHINES"];
        break;
        case '15' :
          var optionArray = ["|--Select--","15|WATERJET CUTTING MACHINES"];
        break;
        case '16' :
          var optionArray = ["|--Select--","16|BLOW MOLDING MACHINES"];
        break;
        case '17' :
          var optionArray = ["|--Select--","17|EXTRUSION - SINGLE SCREW EXTRUDERS"];
        break;
        case '18' :
          var optionArray = ["|--Select--","18|EXTRUSION – TWIN SCREW EXTRUDERS"];
        break;
        case '19' :
          var optionArray = ["|--Select--","19|INJECTION MOLDING MACHINES"];
        break;
        case '20' :
          var optionArray = ["|--Select--","20|PLASTIC MACHINES – OTHER"];
        break;
        case '21' :
          var optionArray = ["|--Select--","21|PLASTIC COMPACTORS / RECYCLING MACHINES"];
        break;
        case '22' :
          var optionArray = ["|--Select--","22|COMPRESORS"];
        break;
        case '23' :
          var optionArray = ["|--Select--","23|FORKLIFT"];
        break;
        case '24' :
          var optionArray = ["|--Select--","24|CRANES"];
        break;
        case '25' :
          var optionArray = ["|--Select--","62|ABB","63|FANUC","64|KUKA","65|OTHERS"];
        break;

        default: optionArray = ["0|--Select--"];
        break;

      }


      for(var option in optionArray){
        var pair = optionArray[option].split("|");
        var newOption = document.createElement("option");
        newOption.value = pair[0];
        newOption.innerHTML = pair[1];
        s2.options.add(newOption);
      }

  }

  function populateSpec(s1,s2){
      var s1 = document.getElementById(s1);
      var s2 = document.getElementById(s2);
      s2.innerHTML = "";
      switch(s1.value){


        case '10' :
          var optionArray = ["bar_length|Bar length","max_bar_diameter|Max bar Ø","spindle_type|Spindle Type","number_of_tubes|Number of tubes","tubes_diameter|Tubes Ø","type_of_loader|Type of loader","max_spindle_speed|Max. spindle speed","motor_power|Motor power"];
        break;
        case '13' :
          var optionArray = ["x_travel|X travel","y_travel|Y travel","z_travel|Z travel","cnc|CNC","type_of_cnc|Type of CNC","metal_length|Metal length","metal_width|Metal width","power|Power","type|Type","max_thickness|Max. thickness","workpiece_weight|Workpiece weight","travel_speed|Travel speed","option_to_cut_tubes|Option to cut tubes"];
        break;
        case '14' :
          var optionArray = ["cnc|CNC","type_of_cnc|Type of CNC","metal_length|Metal length","metal_width|Metal width","max_thickness|Max thickness","number_of_heads|Number of heads","type_of_source|Type of source","source_model|Source model","power|Power"];
        break;
        case '15' :
          var optionArray = ["cnc|CNC","type_of_cnc|Type of CNC","metal_length|Metal length","metal_width|Metal width","max_thickness|Max thickness","number_of_heads|Number of heads","working_pressure|Working pressure","sand_recuperation|Sand recuperation","power|Power"];
        break;
        case '16' :
          var optionArray = ["volume|Volume","extruder_diameter|Extruder diameter","number_of_heads|Number of heads","number_of_stations|Number of stations","closing_stroke|Closing stroke"];
        break;
        case '17' :
          var optionArray = ["screw_diameter|Screw diameter","plastifying_capacity|Plastifying capacity","output|Output","screw_speed|Screw speed","heating_zones|Heating zones"];
        break;
        case '18' :
          var optionArray = ["screw_diameter|Screw diameter","plastifying_capacity|Plastifying capacity","output|Output","screw_speed|Screw speed","heating_zones|Heating zones"];
        break;
        case '19' :
          var optionArray = ["clamping_force|Clamping force","tie_bar_spacing_h|Tie bar spacing (H)","tie_bar_spacing_v|Tie bar spacing (V)","shot_volume|Shot volume","shot_weight|Shot weight","pressure_on_material|Pressure on material","platen_lenght|Platen lenght","platen_width|Platen width","opening_of_the_press|Opening of the press","mould_size|Mould size"];
        break;
        case '20' :
          var optionArray = ["power|Power"];
        break;
        case '21' :
          var optionArray = ["capacity|Capacity","power|Power","screw_diameter|Screw diameter","recycled_material|Recycled material"];
        break;
        case '22' :
          var optionArray = ["nominal_pressure|Nominal Pressure","output_m3_hour|Output (m3 /hour)","loaded_hours|Loaded hours","power|Power","tank_capacity|Tank capacity","volts|Volts","frequency|Frequency","sound_level|Sound level","dryer_integrated|Dryer integrated"];
        break;
        case '23' :
          var optionArray = ["capacity_in_kg|Capacity in kg","lift_height|Lift height","mast|Mast","fork_length|Fork length"];
        break;
        case '24' :
          var optionArray = ["type_of_crane|Type of Crane","load_capacity_max|Load Capacity max","span|Span","height|Height","capacity_on_top|Capacity on top","arm_length|Arm Length"];
        break;
        case '26' :
          var optionArray = ["max_boring|Max boring Ø","workpiece_length|Workpiece length","center_height|Center height","max_diameter|Max Ø","workpiece_weight|Workpiece weight","spindle_speed|Spindle speed","spindle_bore|Spindle bore"];
        break;
        case '27' :
          var optionArray = ["x_axis|X Axis","y_axis|Y Axis","z_axis|Z Axis","table_length|Table length","table_width|Table width","max_piece_weight|Max piece weight","spindle_diameter|Spindle diameter","max_spindle_speed|Max. spindle speed","spindle_motor_power|Spindle motor power","overhauled|Overhauled","year_of_overhauling|Year of overhauling"];
        break;
        case '28' :
          var optionArray = ["cnc|CNC","x_travel|X travel","y_travel|Y travel","z_travel|Z travel","table_length|Table length","table_width|Table width","max_piece_weight|Max piece weight","spindle_taper|Spindle taper","spindle_diameter|Spindle diameter","max_spindle_speed|Max. spindle speed","spindle_motor_power|Spindle motor power","overhauled|Overhauled","year_of_overhauling|Year of overhauling"];
        break;
        case '29' :
          var optionArray = ["x_axis|X Axis","y_axis|Y Axis","z_axis|Z Axis","table_length|Table length","table_width|Table width","max_piece_weight|Max piece weight","spindle_diameter|Spindle diameter","max_spindle_speed|Max. spindle speed","spindle_motor_power|Spindle motor power","overhauled|Overhauled","year_of_overhauling|Year of overhauling"];
        break;
        case '30' :
          var optionArray = ["cnc|CNC","x_travel|X travel","y_travel|Y travel","z_travel|Z travel","table_length|Table length","table_width|Table width","max_piece_weight|Max piece weight","spindle_diameter|Spindle diameter","max_spindle_speed|Max. spindle speed","spindle_motor_power|Spindle motor power","overhauled|Overhauled","year_of_overhauling|Year of overhauling"];
        break;
        case '31' :
          var optionArray = ["distance_between_centers|Distance between centers","max_length_of_workpiece|Max. length of workpiece","max_weight_between_centers|Max. weight between centers","center_height|Center height","cnc|CNC","type_of_cnc|Type of CNC","grinding_wheel_diameter|Grinding wheel diameter","grinding_wheel_width|Grinding wheel width","grinding_wheel_bore|Grinding wheel bore","grinding_wheel_speed|Grinding wheel speed"];
        break;
        case '32' :
          var optionArray = ["max_grinding_length|Max. grinding length","grinding_diameter|Grinding diameter","exter|Exter","inter|Inter","cnc|CNC","type_of_cnc|Type of CNC","max_swing_diameter|Max. swing diameter","max_height_from_spindle_to_table|Max. height from spindle to table","max_wheel_speed|Max. wheel speed","spindle_motor_power|Spindle motor power"];
        break;
        case '33' :
          var optionArray = ["table_length|Table length","table_width|Table width","manual|Manual","cnc|CNC","type_of_cnc|Type of CNC","max_piece_weight|Max piece weight","grinding_wheel_diameter|Grinding wheel diameter","grinding_wheel_width|Grinding wheel width","grinding_wheel_bore|Grinding wheel bore","grinding_wheel_speed|Grinding wheel speed","power|Power"];
        break;
        case '34' :
          var optionArray = ["x_axis|X Axis","y_axis|Y Axis","z_axis|Z Axis","table_length|Table length","table_width|Table width","max_load_on_table|Max load on table","spindle_taper|Spindle taper","max_spindle_speed|Max. spindle speed","spindle_motor_power|Spindle motor power","overhauled|Overhauled","year_of_overhauling|Year of overhauling"];
        break;
        case '35' :
          var optionArray = ["x_axis|X Axis","y_axis|Y Axis","z_axis|Z Axis","type_of_cnc|Type of CNC","table_length|Table length","table_width|Table width","max_load_on_table|Max load on table","spindle_taper|Spindle taper","max_spindle_speed|Max. spindle speed","spindle_motor_power|Spindle motor power","overhauled|Overhauled","year_of_overhauling|Year of overhauling"];
        break;
        case '36' :
          var optionArray = ["x_axis|X Axis","y_axis|Y Axis","z_axis|Z Axis","table_length|Table length","table_width|Table width","max_load_on_table|Max load on table","spindle_taper|Spindle taper","max_spindle_speed|Max. spindle speed","spindle_motor_power|Spindle motor power","overhauled|Overhauled","year_of_overhauling|Year of overhauling"];
        break;
        case '37' :
          var optionArray = ["x_axis|X Axis","y_axis|Y Axis","z_axis|Z Axis","type_of_cnc|Type of CNC","table_length|Table length","table_width|Table width","max_load_on_table|Max load on table","spindle_taper|Spindle taper","max_spindle_speed|Max. spindle speed","spindle_motor_power|Spindle motor power","overhauled|Overhauled","year_of_overhauling|Year of overhauling"];
        break;
        case '38' :
          var optionArray = ["x_axis|X Axis","y_axis|Y Axis","z_axis|Z Axis","cnc|CNC","type_of_cnc|Type of CNC","table_length|Table length","table_width|Table width","max_load_on_table|Max load on table","spindle_taper|Spindle taper","max_spindle_speed|Max. spindle speed","spindle_motor_power|Spindle motor power"];
        break;
        case '39' :
          var optionArray = ["x_axis|X Axis","y_axis|Y Axis","z_axis|Z Axis","table_length|Table length","table_width|Table width","max_load_on_table|Max load on table","spindle_taper|Spindle taper","max_spindle_speed|Max. spindle speed","spindle_motor_power|Spindle motor power","overhauled|Overhauled","year_of_overhauling|Year of overhauling"];
        break;
        case '40' :
          var optionArray = ["x_axis|X Axis","y_axis|Y Axis","z_axis|Z Axis","type_of_cnc|Type of CNC","table_length|Table length","table_width|Table width","max_load_on_table|Max load on table","spindle_taper|Spindle taper","max_spindle_speed|Max. spindle speed","spindle_motor_power|Spindle motor power","overhauled|Overhauled","year_of_overhauling|Year of overhauling"];
        break;
        case '41' :
          var optionArray = ["x_axis|X Axis","y_axis|Y Axis","z_axis|Z Axis","cnc|CNC","table_length|Table length","table_width|Table width","max_piece_weight|Max piece weight","automatic_feed|Automatic feed","number_of_tool_pockets|Number of tool pockets","spindle_taper|Spindle taper","max_spindle_speed|Max. spindle speed","spindle_motor_power|Spindle motor power","rotary_table|Rotary table","number_of_pallets|Number of pallets"];
        break;
        case '42' :
          var optionArray = ["x_axis|X Axis","y_axis|Y Axis","z_axis|Z Axis","type|Type","type_of_axis|Type of axis","cnc|CNC","table_length|Table length","table_width|Table width","max_piece_weight|Max piece weight","automatic_feed|Automatic feed","number_of_tool_pockets|Number of tool pockets","spindle_taper|Spindle taper","max_spindle_speed|Max. spindle speed","spindle_motor_power|Spindle motor power","rotary_table|Rotary table","number_of_pallets|Number of pallets"];
        break;
        case '43' :
          var optionArray = ["x_axis|X Axis","y_axis|Y Axis","z_axis|Z Axis","type|Type","type_of_axis|Type of axis","cnc|CNC","table_length|Table length","table_width|Table width","max_piece_weight|Max piece weight","automatic_feed|Automatic feed","number_of_tool_pockets|Number of tool pockets","spindle_taper|Spindle taper","max_spindle_speed|Max. spindle speed","spindle_motor_power|Spindle motor power","rotary_table|Rotary table","number_of_pallets|Number of pallets"];
        break;
        case '44' :
          var optionArray = ["max_diameter|Max Ø","plate_diameter|Plate diameter","working_height|Working height","max_piece_weight|Max piece weight","rotation_speed|Rotation speed","motor_power|Motor power","overhauled|Overhauled","year_of_overhauling|Year of overhauling"];
        break;
        case '45' :
          var optionArray = ["max_turning_length|Max. turning length","max_turning_diameter|Max turning Ø","cnc|CNC","type_of_cnc|Type of CNC","max_diameter_above_the_bed|Max Ø above the bed","diameter_above_transversing_slide|Ø above transversing slide","distance_between_centers|Distance between centers","center_height|Center height","diameter_in_the_gap|Diameter in the gap","faceplate_diameter|Faceplate diameter","workpiece_weight|Workpiece weight","spindle_nose|Spindle nose","spindle_bore|Spindle bore","positions_number_on_turret|Positions number on turret","turning_speed|Turning speed","spindle_motor_power|Spindle motor power"];
        break;
        case '46' :
          var optionArray = ["max_diameter|Max. Ø","number_of_spindles|Number of spindles","max_length|Max length","type|Type","type_of_cnc|Type of CNC","number_of_axes|Number of axes","number_of_tools|Number of tools","spindle_speed|Spindle speed","spindle_power|Spindle power"];
        break;
        case '47' :
          var optionArray = ["max_diameter|Max. Ø","max_length|Max length","type_of_cnc|Type of CNC","number_of_axes|Number of axes","number_of_c_axes|Number of C axes","type_of_main_spindle|Type of main spindle","type_of_back_spindle|Type of back spindle","number_of_tools|Number of tools","number_of_rotating_tools|Number of rotating tools","spindle_speed|Spindle speed","spindle_power|Spindle power"];
        break;
        case '48' :
          var optionArray = ["power_in_tons|Power in tons","table_length|Table length","table_width|Table width","ram_stroke|Ram stroke","number_of_strokes_min|Number of strokes/min","motor_power|Motor power"];
        break;
        case '49' :
          var optionArray = ["capacity|Capacity","type|Type","ram_stroke|Ram stroke","number_of_strokes_min|Number of strokes/min","motor_power|Motor power"];
        break;
        case '50' :
          var optionArray = ["power_in_tons|Power in tons","type|Type","table_length|Table length","table_width|Table width","ram_stroke|Ram stroke","number_of_strokes_min|Number of strokes/min","working_speed|Working speed","distance_between_columns|Distance between columns","motor_power|Motor power"];
        break;
        case '51' :
          var optionArray = ["table_length|Table length","table_width|Table width","ram_stroke|Ram stroke","number_of_strokes_min|Number of strokes/min","screw_diameter|Screw diameter","motor_power|Motor power"];
        break;
        case '52' :
          var optionArray = ["power_in_tons|Power in tons","type|Type","table_length|Table length","table_width|Table width","ram_stroke|Ram stroke","number_of_strokes_min|Number of strokes/min","working_speed|Working speed","distance_between_columns|Distance between columns","motor_power|Motor power"];
        break;
        case '53' :
          var optionArray = ["power_in_tons|Power in tons","type|Type","table_length|Table length","table_width|Table width","ram_stroke|Ram stroke","number_of_strokes_min|Number of strokes/min","working_speed|Working speed","distance_between_columns|Distance between columns","motor_power|Motor power"];
        break;
        case '54' :
          var optionArray = ["power_in_tons|Power in tons","type|Type","table_length|Table length","table_width|Table width","ram_stroke|Ram stroke","number_of_strokes_min|Number of strokes/min","motor_power|Motor power"];
        break;
        case '55' :
          var optionArray = ["strength|Strength","folding_length|Folding length","distance_between_uprights|Distance between uprights","throat|Throat","vertical_stroke|Vertical stroke","number_of_strokes_min|Number of strokes/min","axis_number|Axis number","hole|Hole","motor_power|Motor power"];
        break;
        case '56' :
          var optionArray = [ "cnc|CNC","strength|Strength","folding_length|Folding length","distance_between_uprights|Distance between uprights","throat|Throat","vertical_stroke|Vertical stroke","number_of_strokes_min|Number of strokes/min","axis_number|Axis number","hole|Hole","motor_power|Motor power"];
        break;
        case '57' :
          var optionArray = ["power_in_tons|Power in tons","metal_length|Metal length","metal_width|Metal width","max_thickness|Max thickness","length_with_repositioning|Length with repositioning","metal_width_with_repositioning|Metal width with repositioning","max_punching_diameter|Max Punching diameter","number_of_strokes_min|Number of strokes/min","workpiece_weight|Workpiece weight","number_of_tools|Number of tools"];
        break;
        case '58' :
          var optionArray = ["type_of_cnc|Type of CNC","power_in_tons|Power in tons","metal_length|Metal length","metal_width|Metal width","max_thickness|Max thickness","length_with_repositioning|Length with repositioning","metal_width_with_repositioning|Metal width with repositioning","max_punching_diameter|Max Punching diameter","number_of_strokes_min|Number of strokes/min","workpiece_weight|Workpiece weight","number_of_tools|Number of tools"];
        break;
        case '59' :
          var optionArray = ["cnc|CNC","cutting_length|Cutting length","max_cutting_thickness|Max cutting thickness","max_cutting_angle|Max cutting angle","min_cutting_angle|Min cutting angle","back_gauge_type|Back gauge type","back_gauge_adjustment|Back gauge adjustment","max_blade_gap_adjustment|Max blade gap adjustment","min_blade_gap_adjustment|Min blade gap adjustment","sheet_support|Sheet support"];
        break;
        case '60' :
          var optionArray = ["cutting_length|Cutting length","max_cutting_thickness|Max cutting thickness","max_cutting_angle|Max cutting angle","min_cutting_angle|Min cutting angle","back_gauge_type|Back gauge type","back_gauge_adjustment|Back gauge adjustment","max_blade_gap_adjustment|Max blade gap adjustment","min_blade_gap_adjustment|Min blade gap adjustment","sheet_support|Sheet support"];
        break;
        case '61' :
          var optionArray = ["cutting_length|Cutting length","max_cutting_thickness|Max cutting thickness","max_cutting_angle|Max cutting angle","min_cutting_angle|Min cutting angle","back_gauge_type|Back gauge type","back_gauge_adjustment|Back gauge adjustment","max_blade_gap_adjustment|Max blade gap adjustment","min_blade_gap_adjustment|Min blade gap adjustment","sheet_support|Sheet support"];
        break;
        case '62' :
          var optionArray = ["application_type|Application type","number_of_axes|Number of axes","reach_in_mm|Reach in mm","payload_in_kg|Payload in kg","control_type|Control type","treated_against_heat|Treated against heat","treated_for_clean_room|Treated for Clean room"];
        break;
        case '63' :
          var optionArray = ["application_type|Application type","number_of_axes|Number of axes","reach_in_mm|Reach in mm","payload_in_kg|Payload in kg","control_type|Control type","treated_against_heat|Treated against heat","treated_for_clean_room|Treated for Clean room"];
        break;
        case '64' :
          var optionArray = ["application_type|Application type","number_of_axes|Number of axes","reach_in_mm|Reach in mm","payload_in_kg|Payload in kg","control_type|Control type","treated_against_heat|Treated against heat","treated_for_clean_room|Treated for Clean room"];
        break;
        case '65' :
          var optionArray = ["application_type|Application type","number_of_axes|Number of axes","reach_in_mm|Reach in mm","payload_in_kg|Payload in kg","control_type|Control type","treated_against_heat|Treated against heat","treated_for_clean_room|Treated for Clean room"];
        break;
        
      }


      for(var option in optionArray){
        var pair = optionArray[option].split("|");

        var newDiv = document.createElement("div");
        newDiv.className = 'element';
        s2.appendChild(newDiv);

        var newLabel = document.createElement("label");
        var t = document.createTextNode(pair[1] + ' :');
        newLabel.setAttribute('for',pair[0]);
        newLabel.appendChild(t);
        newDiv.appendChild(newLabel);

        var newInput = document.createElement("input");
        newInput.type = 'text';
        newInput.className = "form-control";
        newInput.name = pair[0];
        newInput.placeholder = pair[1] + '...';
        newDiv.appendChild(newInput);

      }

  }


