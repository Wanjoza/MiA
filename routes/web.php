<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(
[
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
],
function()
{
	Route::get('/', 'PagesController@home');

	Route::get('home', 'PagesController@home');

	Route::get('about', 'PagesController@about');

	Route::get('contact', 'PagesController@contact');

	Route::get('buy', 'PagesController@buy');

	Route::get('sell', 'PagesController@sell');

	Route::get('service', 'PagesController@service');

	Route::get('search', 'PagesController@search');

	Route::get('{products?}/search', 'PagesController@search');

	Route::get('contact_seller/{item}/search', 'PagesController@search');

	Route::get('contact_seller/{item}/{trans?}', 'PagesController@contact_seller')->name('page.contact_seller');

	Route::get('product/{item}', 'PagesController@product');

	Route::get('products/{category}', 'PagesController@products');

	Route::get('borers', 'CategoryController@borers');

	Route::get('grinding_machines', 'CategoryController@grinding_machines');

	Route::get('milling_machines', 'CategoryController@milling_machines');

	Route::get('machining_centers', 'CategoryController@machining_centers');

	Route::get('lathes', 'CategoryController@lathes');

	Route::get('forging_machines', 'CategoryController@forging_machines');

	Route::get('presses', 'CategoryController@presses');

	Route::get('robots', 'CategoryController@robots');
});



Route::get('admin', 'AdminController@index');

Route::get('show_cat', 'ItemController@show_cat')->name('item.show_cat');

Route::resource('item','ItemController');

Route::post('csv/upload', 'CsvController@upload');

// Messages routes


Route::resource('serviceMessage','ServiceMessageController',['only' => [
    'index', 'show', 'store', 'destroy'
]]);

Route::resource('contactMessage','ContactMessageController',['only' => [
    'index', 'show', 'store', 'destroy'
]]);

Route::resource('sellerMessage','SellerMessageController',['only' => [
    'index', 'show', 'store', 'destroy'
]]);

Auth::routes();

