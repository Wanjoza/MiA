<?php

namespace App\Http\Controllers;

use App\ContactMessage;
use Illuminate\Http\Request;
use App\Http\Requests\ContactMessageRequest;
use Carbon\Carbon;
use GuzzleHttp\Client;

class ContactMessageController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth')->except('store');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ContactMessage $message)
    {
        $messages = $message->orderBy('created_at','desc')->paginate(5);

        return view('contactMessage.index', compact('messages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactMessage $contactMessage, ContactMessageRequest $request){

        $token = $request->input('g-recaptcha-response');

        if ($token) {
            
            $client = new Client();
            $response = $client->post('https://www.google.com/recaptcha/api/siteverify',[ 'form_params' => array(
                        'secret'     =>  '6LeSVTQUAAAAAE_hob4H7kpyzTzSZDMbSXjwh7aI',
                        'response'   => $token
                    ) 
            ]);

            $result = json_decode($response->getBody()->getContents());

            if ($result->success) {
                
                $contactMessage->f_name = $request->f_name;

                $contactMessage->l_name = $request->l_name;

                $contactMessage->phone = $request->phone;

                $contactMessage->company = $request->company;

                $contactMessage->email = $request->email;

                $contactMessage->title = $request->title;

                $contactMessage->message = $request->message;

                $contactMessage->save();

                flash()->success('Success!', 'You have successfully sent message.');
                
                return redirect()->back();

            } else {

                flash()->error('Error', 'You are probbly a robot!');

                return redirect()->back();
            }

            
        } else {

            flash()->error('Error', 'No captcha code entered');

            return redirect()->back();
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContactMessage  $contactMessage
     * @return \Illuminate\Http\Response
     */
    public function show(ContactMessage $contactMessage)
    {
        $message = $contactMessage->all()->where('id', $contactMessage->id)->first();

        return view('contactMessage.show', compact('message'));
    }

   
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactMessage  $contactMessage
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactMessage $contactMessage)
    {
        $contactMessage->destroy($contactMessage->id);

        flash()->success('Deleted!', 'You have successfully deleted message.');

        return redirect()->back();
    }
}
