<?php

namespace App\Http\Controllers;

use App\SellerMessage;
use Illuminate\Http\Request;
use App\Http\Requests\SellerMessageRequest;
use Carbon\Carbon;
use App\ItemPhoto;
use App\Item;
use GuzzleHttp\Client;

class SellerMessageController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth')->except('store');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SellerMessage $sellerMessage)
    {
        $messages = $sellerMessage->orderBy('created_at','desc')->paginate(5);

        return view('sellerMessage.index', compact('messages'));
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SellerMessage $sellerMessage, SellerMessageRequest $request){
        

        $token = $request->input('g-recaptcha-response');

        if ($token) {

            $client = new Client();
            $response = $client->post('https://www.google.com/recaptcha/api/siteverify',[ 'form_params' => array(
                        'secret'     =>  '6LeSVTQUAAAAAE_hob4H7kpyzTzSZDMbSXjwh7aI',
                        'response'   => $token
                    ) 
            ]);

            $result = json_decode($response->getBody()->getContents());

            if ($result->success) {

                
                $sellerMessage->f_name = $request->f_name;

                $sellerMessage->l_name = $request->l_name;

                $sellerMessage->phone = $request->phone;

                $sellerMessage->company = $request->company;

                $sellerMessage->email = $request->email;

                $sellerMessage->title = $request->title;

                $sellerMessage->message = $request->message;

                $sellerMessage->item_id = $request->item_id;

                $sellerMessage->trans_src = $request->trans_src;

                $sellerMessage->save();

                flash()->success('Success!', 'You have successfully sent message.');
                
                return redirect()->back();

            } else {

                flash()->error('Error', 'You are probbly a robot!');

                return redirect()->back();
            }

            
        } else {

            flash()->error('Error', 'No captcha code entered');

            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SellerMessage  $sellerMessage
     * @return \Illuminate\Http\Response
     */
    public function show(SellerMessage $sellerMessage, Item $item)
    {
        $message = $sellerMessage->all()->where('id', $sellerMessage->id)->first();

        $item_id = $sellerMessage->item_id;

        $trans_src = $sellerMessage->trans_src;

        $item = $item->where('id',$item_id)->first();
        

        return view('sellerMessage.show', compact('message','item','trans_src'));
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SellerMessage  $sellerMessage
     * @return \Illuminate\Http\Response
     */
    public function destroy(SellerMessage $sellerMessage)
    {
        $sellerMessage->destroy($sellerMessage->id);

        flash()->success('Deleted!', 'You have successfully deleted message.');

        return redirect()->back();
    }
}
