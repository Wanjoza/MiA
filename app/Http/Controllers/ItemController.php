<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Http\Requests\UpdateItemRequest;
use App\ItemPhoto;
use App\Item;
use App\Category;
use File;
use Carbon\Carbon;

class ItemController extends Controller{

    public function __construct()
    {
        $this->middleware('auth');
    }

    // Show all Items from selected category

    public function show_cat(Request $request){

      $items = new Item;

      $selected_id = $request->category_id;

      $title = Category::where('id', $selected_id)->first(); 

      $queries = [];


      if (request()->has('category_id')) {
         
          $items = $items->where('category_id',request('category_id'));
          $queries['category_id'] = request('category_id');

      }
      
      $items = $items->paginate(9)->appends($queries);
      
    	return view('item.index',compact('items','title'));

    }

    // Show item

    public function show(Item $item){

      // Get all item specifications

        $item_specs = collect( array_filter( $item->getAttributes()))
                        ->except('id','category_id','item_title','manufacturer','model','year','location',
                          'product_id','price','url','description','created_at','updated_at')
                        ->all();

        // Fix column names

        $keys = array_keys($item_specs);
        $values = array_values($item_specs);
        
        foreach ($keys as $key) {
            $item_keys[] = ucfirst(implode(' ',explode('_', $key)));
        }

        $item_specs = array_combine($item_keys, $values);

      return view('item.show',compact('item','item_specs'));

    }

    // Create Item view

    public function create(){


    	return view('item.create');
    }


    // Store item via form

    public function store(ItemRequest $request, ItemPhoto $item_photo, Item $item){

              

          $collection = collect($request)->except('select_1','select_2','item_photos','_token')->all();

          $item_id = Item::firstOrCreate($collection);

          $photos = $request->file('item_photos');
          
          foreach($photos as $photo){

              $photo_name = $item_photo->fromForm($photo); 

              ItemPhoto::create([
                  'item_id' => $item_id->id,
                  'photo_path' => $photo_name
              ]);

          }

          flash()->success('Success!', 'You have successfully inserted item.');

          return redirect()->back();
    	
    }

    
      

    // Edit items

    public function edit(Item $item){

        $item = Item::where('id', $item->id)->first();
        $photos = ItemPhoto::where('item_id',$item->id)->get();
        

        return view('item.edit', compact('item','photos'));

    }


    // Update items
    public function update(UpdateItemRequest $request, ItemPhoto $item_photo, Item $item){

  
          $collection = collect($request)->except('delete_photo','item_photos','_method','_token')->all();
          

          Item::where('id', $item->id )->update($collection);

          $photos = $request->file('item_photos');
          
          if (!empty($photos)) {

              foreach($photos as $photo){

                  $photo_name = $item_photo->fromForm($photo); 

                  ItemPhoto::create([
                      'item_id' => $item->id,
                      'photo_path' => $photo_name
                  ]);

              }
          }

          $photo_ids = $request->delete_photo;

          if (!empty($photo_ids)) {

              foreach($photo_ids as $photo_id){
                
                  $photo = ItemPhoto::where('id',$photo_id)->first();
                  $path = explode('/', $photo->photo_path);
                  $photo_path = implode('\\', $path);
                  
                  File::delete(public_path($photo_path));
                  $photo->destroy($photo->id);
              }
          }
          



          flash()->success('Success!', 'You have successfully updated item.');

          return redirect()->back();

    }

    public function destroy(Item $item){

        $photos = ItemPhoto::where('item_id', $item->id)->get()->all(); 

       
        // Delete files from images directory
        foreach ($photos as $photo) {

            $path = explode('/', $photo->photo_path);
            $photo_path = implode('\\', $path); 

            File::delete(public_path($photo_path));
            $photo->destroy($photo->id);
        };

        //Delete Item
        $item->destroy($item->id);

        flash()->success('Deleted!', 'You have successfuly deleted Item!');

        return redirect()->back();
    }


}
