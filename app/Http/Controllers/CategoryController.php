<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ItemPhoto;
use App\Item;
use App\Category;


class CategoryController extends Controller
{


    public function borers(Item $item){

        $collection = $item->whereIn('category_id',[26, 27, 28, 29, 30])->get();
        
        if ($collection->count() > 4 ) {
            $items = $collection->random(4)->all();
        }else{
            
          $items = $collection->all();
        }
        
        return view('pages.metal_cutting_machines.borers',compact('items'));
    }



    public function grinding_machines(Item $item){

        $collection = $item->whereIn('category_id',[31, 32, 33])->get();
        
        if ($collection->count() > 4 ) {
            $items = $collection->random(4)->all();
        }else{
            
          $items = $collection->all();
        }

        return view('pages.metal_cutting_machines.grinding_machines',compact('items'));
    }

    public function milling_machines(Item $item){

        $collection = $item->whereIn('category_id',[34, 35, 36, 37, 38, 39, 40])->get();
        
        if ($collection->count() > 4 ) {
            $items = $collection->random(4)->all();
        }else{
            
          $items = $collection->all();
        }
    
        return view('pages.metal_cutting_machines.milling_machines',compact('items'));
    }


    public function machining_centers(Item $item){

        $collection = $item->whereIn('category_id',[41, 42, 43])->get();
        
        if ($collection->count() > 4 ) {
            $items = $collection->random(4)->all();
        }else{
            
          $items = $collection->all();
        }

    
        return view('pages.metal_cutting_machines.machining_centers',compact('items'));
    }


    public function lathes(Item $item){

        $collection = $item->whereIn('category_id',[44, 45, 46, 47])->get();
        
        if ($collection->count() > 4 ) {
            $items = $collection->random(4)->all();
        }else{
            
          $items = $collection->all();
        }

        return view('pages.metal_cutting_machines.lathes',compact('items'));
    }

    

    public function forging_machines(Item $item){
    
        $collection = $item->whereIn('category_id',[48, 49, 50, 51])->get();
        
        if ($collection->count() > 4 ) {
            $items = $collection->random(4)->all();
        }else{
            
          $items = $collection->all();
        }

        return view('pages.sheet_metal_shaping.forging_machines',compact('items'));
    }

    public function presses(Item $item){

        $collection = $item->whereIn('category_id',[52, 53, 54, 55, 56, 57, 58, 59, 60, 61])->get();
        
        if ($collection->count() > 4 ) {
            $items = $collection->random(4)->all();
        }else{
            
          $items = $collection->all();
        }
    
        return view('pages.sheet_metal_shaping.presses',compact('items'));
    }

    public function robots(Item $item){

        $collection = $item->whereIn('category_id',[62, 63, 64, 65])->get();
        
        if ($collection->count() > 4 ) {
            $items = $collection->random(4)->all();
        }else{
            
          $items = $collection->all();
        }

        return view('pages.miscellaneous_industrial_machines.robots',compact('items'));
    }

}
