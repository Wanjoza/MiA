<?php

namespace App\Http\Controllers;

use App\ServiceMessage;
use Illuminate\Http\Request;
use App\Http\Requests\ServiceMessageRequest;
use Carbon\Carbon;
use GuzzleHttp\Client;

class ServiceMessageController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth')->except('store');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ServiceMessage $message)
    {
        $messages = $message->orderBy('created_at','desc')->paginate(5);

        return view('serviceMessage.index', compact('messages'));
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceMessage $serviceMessage, ServiceMessageRequest $request){
        
        $token = $request->input('g-recaptcha-response');

        if ($token) {
            
            $client = new Client();
            $response = $client->post('https://www.google.com/recaptcha/api/siteverify',[ 'form_params' => array(
                        'secret'     =>  '6LeSVTQUAAAAAE_hob4H7kpyzTzSZDMbSXjwh7aI',
                        'response'   => $token
                    ) 
            ]);

            $result = json_decode($response->getBody()->getContents());

            if ($result->success) {
                
                $serviceMessage->f_name = $request->f_name;

                $serviceMessage->l_name = $request->l_name;

                $serviceMessage->phone = $request->phone;

                $serviceMessage->company = $request->company;

                $serviceMessage->email = $request->email;

                $serviceMessage->title = $request->title;

                $serviceMessage->message = $request->message;

                $serviceMessage->save();

                flash()->success('Success!', 'You have successfully sent message.');
                
                return redirect()->back();

            } else {

                flash()->error('Error', 'You are probbly a robot!');

                return redirect()->back();
            }

            
        } else {

            flash()->error('Error', 'No captcha code entered');

            return redirect()->back();
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceMessage  $serviceMessage
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceMessage $serviceMessage)
    {
        $message = $serviceMessage->all()->where('id', $serviceMessage->id)->first();

        return view('serviceMessage.show', compact('message'));
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceMessage  $serviceMessage
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceMessage $serviceMessage)
    {
        $serviceMessage->destroy($serviceMessage->id);

        flash()->success('Deleted!', 'You have successfully deleted message.');

        return redirect()->back();
    }
}
