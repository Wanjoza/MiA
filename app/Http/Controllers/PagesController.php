<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ItemPhoto;
use App\Item;
use App\Category;
use Illuminate\Support\Facades\Input;

class PagesController extends Controller
{


    public function home(Item $item){

         $count = count($item->get());


      if ( $count > 12) {
          
          $items = $item->get()->random(12);

      }else{

          $items = $item->get();
      }

    
    	return view('pages.home',compact('items'));
    }




    public function about(){
    
    	return view('pages.about');
    }




    public function contact(){
    
    	return view('pages.contact');
    }

    public function buy(Item $item){
    
        $count = count($item->get());
        
      if ( $count > 4) {
          
          $items = $item->get()->random(4);

      }else{

          $items = $item->get();
      }

      
    	return view('pages.buy', compact('items'));
    }

    public function sell(){
    
    	return view('pages.sell');
    }

    public function service(){
    
      return view('pages.service');
    }

    public function product(Item $item){

        // Get all item specifications

        $item_specs = collect( array_filter( $item->getAttributes()))
                        ->except('id','category_id','item_title','manufacturer','model','year','location',
                          'product_id','price','url','description','created_at','updated_at')
                        ->all();

        // Fix column names

        $keys = array_keys($item_specs);
        $values = array_values($item_specs);
        
        foreach ($keys as $key) {
            $item_keys[] = ucfirst(implode(' ',explode('_', $key)));
        }

        $item_specs = array_combine($item_keys, $values);

        // Get items for random same category items

        $count = count($item->where('category_id',$item->category_id)->get());
        
        if ($count > 4 ) {

            $category_items = $item->where('category_id',$item->category_id)->get()->random(4);

        }else{

          $category_items = $item->where('category_id',$item->category_id)->get();
        }
        

        return view('pages.product', compact('item','category_items','item_specs'));
    }



    public function products(Category $category, Item $item){

        $title = $category->where('id', $category->parent_id)->first();

        $items = $item->where('category_id', $category->id)->paginate(12);

        return view('pages.products',compact('category','items','title'));
    }


    public function search(Request $request, Item $item){

        $term = $request->search;
        
        $items = $item->where('item_title', 'like', '%'.$term.'%')
                      ->orderBy('manufacturer')
                      ->paginate(12)
                      ->appends(Input::except('page'));
        
        return view('pages.search_results',compact('items','term'));
    }



    public function contact_seller(Item $item, $trans = null){

        if($trans == 1 || $trans == 2 || $trans == 3 || $trans == 4){
          $trans_src = '/img/trans_' . $trans . '.jpg';
        }else{
          $trans_src = null;
        }
 
        return view('pages.contact_seller',compact('item','trans_src'));

    }


}
