<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\ItemPhoto;
use App\Item;
use App\Flag;
use File;
use Validator;
use Exception;
use Schema;
use Redirect;
use Config;

class CsvController extends Controller
{
    // Store items via CSV

    public function upload(Request $request){

    //Basic validation and extension check
          set_time_limit(-1);

          $csv_file = $request->file('imported_file');

          $validator = Validator::make($request->all(), [
               'imported_file' => 'required'
          ]);

          $validator->after(function($validator) use ($csv_file) {

                   if ($csv_file->getClientOriginalExtension() !== 'csv') {
                   $validator->errors()->add('field', 'File type is invalid - only CSV is allowed');
               }
          });


          if ($validator->fails()) {
               return Redirect::to(route('item.create'))
                           ->withErrors($validator);
          }

          //checking difference between file columns and database columns

          $first_row = Excel::load( $csv_file , function($reader){})
                              ->takeRows(1)
                              ->first()
                              ->toArray();


          $first_row = collect($first_row)
                              ->except('item_photos')
                              ->all();
          
          $db_columns = collect(Schema::getColumnListing('items'))
                                ->except(0)
                                ->all();
                        
          $diff = implode(', ', array_diff(array_keys($first_row), $db_columns));
           
          if ($diff !== "") {
              $m = "Error! These columns ' " . $diff . " ' from CSV file are different from columns in database.";
              $validator->errors()->add('field', $m);

              return Redirect::to(route('item.create'))
                           ->withErrors($validator);
          }
             
          
          try {
               $fname = time() . '_' . $csv_file->getClientOriginalName();
               $full_path = Config::get('filesystems.disks.local.root');
               $flag_table = Flag::firstOrNew(['file_name'=>$fname]);
               $flag_table->imported = false; //file was not imported
               $csv_file->move( $full_path, $fname );
               $flag_table->save();
               
          }catch(\Exception $e){
               return Redirect::to(route('item.create'));
          }
          
          // Load file into Excel package 
          $file = Flag::where('imported','=', false)
                   ->orderBy('created_at', 'DESC')
                   ->first();

          $file_path = Config::get('filesystems.disks.local.root') . '\\' .$file->file_name;
          
          // Check if is valid CSV

           Excel::load($file_path, function($reader) use($file) {
               $rows = $reader->toArray();
              
               foreach ($rows as $row) {

                  if (empty(array_filter($row))) {

                      throw new Exception("Error! CSV is unreadable! Please check...", 1);
                    }
                }
               
               
               $objWorksheet = $reader->getActiveSheet();
               $file->total_rows = $objWorksheet->getHighestRow() - 1; //exclude the heading
               $file->save();
           });

          

          Excel::filter('chunk')
             ->selectSheetsByIndex(0)
             ->load($file_path)
             ->chunk(100, function($result) use ($file) {

              $rows = $result->toArray();
                 
              $counter = 0;


               foreach ($rows as $k => $row) {

                  
                   foreach ($row as $c => $cell) {
                       $rows[$k][$c] = $cell;
                   }

                   $collection = collect($rows[$k])->except('item_photos')->all();

                   $photos = explode('|',collect($rows[$k])->pull('item_photos'));


                   $item_id = Item::firstOrCreate($collection);

                   foreach($photos as $photo){ 

                        ItemPhoto::create([
                            'item_id' => $item_id->id,
                            'photo_path' => $photo
                        ]);
                   }
                   
                   $counter++;
               }

               $file = $file->fresh(); //reload from the database
               $file->imported = true;
               $total = $file->rows_imported + $counter;
               $file->rows_imported = $total;
               $file->save();



              });

              // Delete failed

              $failed = Flag::where('rows_imported','=', false)
                        ->orderBy('created_at', 'DESC')
                        ->first(); 
              if (!is_null($failed)) {

                File::delete(Config::get('filesystems.disks.local.root') . '\\' .$failed->file_name);
                $failed->destroy($failed->id);

              }
              

              
              flash()->success('Success!', 'You have successfully inserted items');
                return redirect()->back();

           }
}
