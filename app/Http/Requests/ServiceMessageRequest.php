<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f_name' => 'required|min:3|max:100',
            'l_name' => 'required|min:3|max:100',
            'company' => 'required|min:3|max:100',
            'phone' => 'required|numeric|digits_between:8,15',
            'email' => 'required|email',
            'title' => 'required|min:3|max:100',
            'message' => 'required|min:5|max:1000',
        ];
    }
}
