<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'      => 'required',
            'item_title'       => 'required',
            'manufacturer'     => 'required',
            'model'            => 'required',
            'year'             => 'required|numeric|digits:4',
            'location'         => 'required',
            'product_id'       => 'required',
            'price'            => 'required',
            'item_photos'      => 'required',
            'item_photos.*'    => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

    }
}
