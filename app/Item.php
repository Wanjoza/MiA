<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

    protected $fillable = [
    	'category_id','url','item_title','manufacturer','model','year','location','product_id','price',
    	'x_axis','y_axis','z_axis','x_travel','y_travel','z_travel','cnc','type_of_cnc','max_boring','workpiece_length','max_length',
    	'center_height','max_fi','workpiece_weight','spindle_speed','spindle_power','spindle_nose','spindle_bore','table_length','table_width','max_load_on_table','spindle_taper','max_piece_weight','spindle_diameter','max_spindle_speed','spindle_motor_power',
    	'distance_between_centers','max_length_of_workpiece','max_weight_between_centers','grinding_wheel_diameter','grinding_wheel_width','grinding_wheel_bore','grinding_wheel_speed','max_grinding_length','grinding_diameter','exter','inter','max_swing_diameter','max_height_from_spindle_to_table','max_wheel_speed','manual','power','type','type_of_axis','automatic_feed','number_of_tool_pockets','rotary_table','number_of_pallets','plate_diameter','working_height','rotation_speed','motor_power','max_turning_length','max_turning_fi','max_fi_above_the_bed','fi_above_transversing_slide','diameter_in_the_gap','faceplate_diameter','positions_number_on_turret','turning_speed','number_of_spindles','number_of_axes','number_of_c_axes','type_of_main_spindle','type_of_back_spindle','number_of_tools','number_of_rotating_tools','bar_length','max_bar_fi','number_of_tubes','tubes_fi','type_of_loader','power_in_tons','ram_stroke','number_of_strokes_min','capacity','working_speed','distance_between_columns','screw_diameter','screw_speed','strength','folding_length','distance_between_uprights','throat','vertical_stroke','axis_number','hole','metal_length','metal_width','max_thickness','length_with_repositioning','metal_width_with_repositioning','max_punching_diameter','cutting_length','max_cutting_thickness','max_cutting_angle','min_cutting_angle','back_gauge_type','back_gauge_adjustment','max_blade_gap_adjustment','min_blade_gap_adjustment','sheet_support','travel_speed','option_to_cut_tubes','number_of_heads','type_of_source','source_model','working_pressure','sand_recuperation','volume','extruder_diameter','number_of_stations','closing_stroke','plastifying_capacity','output','heating_zones','clamping_force','tie_bar_spacing_h','tie_bar_spacing_v','shot_volume','shot_weight','pressure_on_material','platen_lenght','platen_width','opening_of_the_press','mould_size','recycled_material','nominal_pressure','output_m3_hour','loaded_hours','tank_capacity','volts','frequency','sound_level','dryer_integrated','capacity_in_kg','lift_height','mast','fork_length','type_of_crane','load_capacity_max','span','height','capacity_on_top','arm_length','application_type','reach_in_mm','payload_in_kg','control_type','treated_against_heat','treated_for_clean_room','overhauled','year_of_overhauling','length_width_height','weight','worked_hours','hours_under_power','at_local_norms','state','status','description'

    ];
 
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function item_photos()
    {
        return $this->hasMany('App\ItemPhoto');
    }
}
