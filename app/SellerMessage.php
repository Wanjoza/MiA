<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerMessage extends Model
{
    protected $fillable = ['f_name','l_name','company','email','phone','title','message','item_id','trans_src'];
}
