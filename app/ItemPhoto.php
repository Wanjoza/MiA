<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Intervention\Image\ImageManagerStatic as Image;
use Exception;

class ItemPhoto extends Model
{
    protected $fillable = ['item_id','url','photo_path'];
 


    public function fromForm(UploadedFile $file)

    {

        if ($file->isValid()) {
            
            $name = time() . $file->getClientOriginalName();
        
            Image::make($file)
                ->resize(980, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })
                ->orientate()
                ->save(public_path('img\item_photos\\' . $name));


            $name = '/img/item_photos/' . $name;

            return $name;        

        }else{

            throw new Exception("Error uploading file!", 1);
        }

    }

    public function item()
    {
        return $this->belongsTo('App\Item');
    }   

}
